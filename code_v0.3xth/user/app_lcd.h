#ifndef __APP_LCD_H__
#define __APP_LCD_H__

#include "type_define.h"

typedef enum
{
	ELEM_NO_SYMB = 0,
	ELEM_MEM_SYMB,  //记忆标志
	ELEM_RING_SYMB,//铃铛标志
	ELEM_NFC_SYMB,//NFC标志
	ELEM_BLE_SYMB,//BLE标志
	ELEM_VOICE_SYMB,//声音标志
	ELEM_CONTROL_SYMB,//质控液标志
	ELEM_WATER_DROP_SYMB,//水滴
	ELEM_S8_SYMB,//血糖值小数点
	ELEM_S9_SYMB,//
	ELEM_S10_SYMB,//
	ELEM_S11_SYMB,//文字:“闹钟”
	ELEM_MMOL_SYMB,//单位：mmol/L 
	ELEM_MGDL_SYMB,//单位：mg/dL
	ELEM_APPLE_SYMB,//图样：苹果
	ELEM_S15_SYMB,//图样：蜡烛
	ELEM_S16_SYMB,//图样：禁止使用刀具
	ELEM_S17_SYMB,//文字:“血铜”
	ELEM_BATTERY_SYMB,//图样:电池
	ELEM_S19_SYMB,//图样:-
	ELEM_S22_SYMB,//文字:“日平均”
	ELEM_S23_SYMB,//时间中间的冒号
	ELEM_S24_SYMB,//文字:“上午”
	ELEM_S25_SYMB,//文字:“下午”
}e_event_em;

typedef struct{
//struct{
	uint8_t updateFlag;//显示一次
	uint8_t stage;//显示状态
	uint8_t blinkFlag;//闪烁标志 
	uint8_t disCnt500ms;//计数
	uint8_t set_start_flag;//是否进入设置状态
	uint8_t time_count;//闪烁处理
	void(* LcdDisplayFuncCb)(void); 
}DisInfo_t;
extern DisInfo_t displayInfo;


uint8_t Get_Main_Stage( void );
void Add_Main_Stage(void);
void Sub_Main_Stage(void);
void Set_Main_Stage(uint8_t temp_n);

void Set_Update_Flag(uint8_t temp_n);

void Set_Main_Count(uint8_t temp_n);
void Set_Time_Reload(void);
uint8_t Get_Set_Start_Flag(void);
void Anti_Set_Start_Flag(void);
void Set_Blink_Flag(uint8_t temp_n);
typedef void (*pLed)(void);

void InitDisplayInfo(void);
//void LcdDisplayCbSet(uint8_t f_state);






#endif