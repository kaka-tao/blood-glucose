#include "main.h"
typedef struct
{
  uint8_t   Run;              //任务状态：Run/Stop
  uint16_t  TimCount;        //时间片周期，用于递减函数
  uint16_t  TRITime;         //时间片周期，用于函数重载
  void (*TaskHook)(void);   //函数指针，保存任务函数地址	
}TASK_COMPONENTS;

//在这里注册任务
static TASK_COMPONENTS g_Task_Comps[] = 
{
        //状态    计数    周期    函数
//        { 0,     100,    100,    TaskProSleep},
        { 0,     100,    100,    TaskLcd},
		    { 0,     100,    100,    TaskButton},
};

#define   TasksMax       (sizeof(g_Task_Comps)/sizeof(g_Task_Comps[0])) 

/**********************************************************
* @brief 系统任务扫描入口
* @param 
* @return 
**********************************************************/
static void TaskHandler(void)
{
	 uint8_t i;
	
   for(i = 0;i < TasksMax;i++)
	 {
	   if(g_Task_Comps[i].Run)
	   {
		    g_Task_Comps[i].Run = 0;//标记清零
		    g_Task_Comps[i].TaskHook();//执行任务
		 }
	 }
	 MainStatePro(&g_SysInfo);
} 
/**********************************************************
* @brief 1ms定时
* @param 
* @return 
**********************************************************/
static void TimerTaskScan(void)
{
	 uint8_t i;
	
	IncGlobalCount();
	for(i = 0;i < TasksMax;i++)
	{
		if(--g_Task_Comps[i].TimCount == 0)
			{
				 //时间标记为1，并重载计数初值
			   g_Task_Comps[i].TimCount = g_Task_Comps[i].TRITime;
			   g_Task_Comps[i].Run = 1;	
			}
	}
}

/**********************************************************
* @brief 注册1ms定时回调函数
* @param 
* @return 
**********************************************************/
static void RegTimer1msInt(void)
{
  Reg1msInt(TimerTaskScan);
}	
/**********************************************************
* @brief 注册2.5ms回调函数
* @param 
* @return 
**********************************************************/
static void RegTimer2_5msInt(void)
{
  Reg2_5msInt(Timer2_5msCallBack);
}
/**********************************************************
* @brief 注册adc中断回调函数
* @param 
* @return 
**********************************************************/
static void RegAdcInt(void)
{
  RegAdcIntFunc(AdcIntCallBack);
}
/**********************************************************
* @brief 唤醒前的各种外设低功耗初始化
* @param 
* @return 
**********************************************************/
void HwBeforeSleepInit(void)
{
  CloseAllPeripheral();
}
/**********************************************************
* @brief 唤醒后的各种外设初始化
* @param 
* @return 
**********************************************************/
void HwAfterSleepInit(void)
{
	GpioInit();
	InitLcd();
	InitUsart();
	AnalogInit();//模拟电路
//	Tc0Init();//RTC
	Tc1Init();//1ms定时
	Tc2Init();//2.5ms定时
	
	//memset(&g_SysInfo,0,sizeof(g_SysInfo));
	ClearGlobalCount();//清零系统计数
}
/**********************************************************
* @brief 系统应用初始化
* @param 
* @return 
**********************************************************/
void AppInit(void)
{
	ClearGlobalCount();//清0系统tick
	RegTimer1msInt();//注册系统定时器
	RegTimer2_5msInt();
	RegAdcInt();//注册Adc中断服务
	InitDisplayInfo();//初始化lcd
	RegButton();//注册按键
	SleepInitSysVar();//初始化全局参数 
	InitMemoryInfo();  //初始化记忆并读取
}

/**********************************************************
* @brief 上电硬件初始化
* @param 
* @return 
**********************************************************/
static void HwInit(void)
{
	InitSysCLK();//系统时钟4M
  
	InitDetGpio();//配置DET引脚为上拉输入
	InitButtonGpio();//配置按键引脚为上拉输入
	Tc1Init();//1ms定时
}

void main(void)
{
	 DisableIrq(); // Interrupt disable

	 AppInit();
	 HwInit();
	
	 //HwAfterSleepInit();
	
   EnableIrq(); // Interrupt enable 
	
   while (1) {
     TaskHandler();
   }
}