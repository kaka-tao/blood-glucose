/***************************************
* bref 该文件对模拟数据进行处理
***************************************/
#include "main.h"

#define TEMP_STEP1        337U
#define TEMP_STEP2        408U
#define TEMP_STEP3        580U
#define TEMP_STEP4        682U
#define TEMP_STEP5        790U
#define TEMP_STEP6        1047U

#define DAC_CAL_WORK      409U
#define DAC_CAL_THIRD     409U

Analog_t  analogInfo;


/**********************************************************
* @brief 对相应通道的ADC取平均值
* @param 
* @return 
**********************************************************/
uint16_t SenseAdAverage(AdcCH_t ch,uint8_t numbers)
{
	uint8_t i = 0;
//	uint16_t tempData = 0;
//	uint16_t max = 0;
//	uint16_t min = 0xffff;
	uint32_t sum = 0;
	
  switch (ch)
	{
	  case CH_EXT_TEMP_SENSOR:
		{
		  AdcConnectTempSensor();
		}
		break;
	  case CH_BATTERY:
		{
		  AdcConnectBattery();
		}
		break;
	  case CH_WORK_OUT:
		{
       AdcConnect_Opa1_WorkOut();
		}
		break;
	  case CH_THIRD_OUT:
		{
       AdcConnect_Opa2_ThirdOut();
		}
		break;
	  case CH_WORK_IN:
		{
       AdcConnectWorkIn();
		}
		break;
	  case CH_THIRD_IN:
		{
       AdcConnectThirdIn();
		}
		break;
		case CH_CALIB_DAC:
		{
		   AdcConnect_CalibDao();
		}
		break;
	}
  for(i = 0;i < 4;i++)
	{
	  sum += ReadAdcSingle();
	}
	sum = 0;
  for(i = 0;i < numbers;i++)
	{
	  sum += ReadAdcSingle();
	}
  return (uint16_t)(sum/numbers);
}
/**********************************************************
* @brief 读取温度
* @param 
* @return 
**********************************************************/
int16_t ReadTempSensor(void)
{
	int32_t   DegC;
	uint16_t  ADCValue = 0;

	ADCValue = SenseAdAverage(CH_EXT_TEMP_SENSOR, 8);

	if (ADCValue <= TEMP_STEP1)           //~5'C
	{
		DegC = (int32_t)ADCValue * 654;

		DegC = (DegC - 170520) / 1000;
		return (int16_t)DegC;
	}
	else if (ADCValue <= TEMP_STEP2)			//5~10
	{
		DegC = (int32_t)ADCValue * 706;

		DegC = (DegC - 188361) / 1000;
		return (int16_t)DegC;
	}
	else if (ADCValue <= TEMP_STEP3)		 	//10~20
	{
		DegC = (int32_t)ADCValue * 598;

		DegC = (DegC - 147612) / 1000;
		return (int16_t)DegC;
	}
	else if (ADCValue <= TEMP_STEP4)		 	//20~25
	{
		DegC = (int32_t)ADCValue * 490;

		DegC = (DegC - 84360) / 1000;
		return (int16_t)DegC;
	}
	else if (ADCValue <= TEMP_STEP5)		 	//25~30
	{
		DegC = (int32_t)ADCValue * 446;

		DegC = (DegC - 54401) / 1000;
		return (int16_t)DegC;
	}
	else if (ADCValue <= TEMP_STEP6)			//30~40
	{
		DegC = (int32_t)ADCValue * 396;

		DegC = (DegC - 15102) / 1000;
		return (int16_t)DegC;
	}
	else if (ADCValue != 0xffff)		      //40~50
	{
		DegC = (int32_t)ADCValue * 355;

		DegC = (DegC + 28674) / 1000;
		return (int16_t)DegC;
	}
	else
		return 0xffff;
}
/**********************************************************
* @brief 获取电池电量
* @param 
* @return 
**********************************************************/
void BatGetLevel(void)
{
	float    adc_value;
	float    battery_level;
	uint16_t nAREF;
  
	nAREF = GetNref();
	if(nAREF <= 17000||nAREF >= 23000){
		nAREF = 20000; 
	}
	SetNref(nAREF);
	analogInfo.batLevel = e_BatLevel_High;

	adc_value = (float)SenseAdAverage(CH_BATTERY, 15);
	battery_level = (BATRATIO * ((adc_value /409500.0f) * (float)nAREF));

	if(battery_level <= (float)e_BatLevel_Shutdown){
		analogInfo.batLevel = e_BatLevel_Shutdown;
	}
	else if(battery_level <= (float)e_BatLevel_Low){
		analogInfo.batLevel = e_BatLevel_Low;
	}
}


