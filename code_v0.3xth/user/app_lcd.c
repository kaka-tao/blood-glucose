/***************************************
* bref 该文件处理显示相关任务
***************************************/
#include "main.h"
//#include "app_lcd.h"

DisInfo_t displayInfo;

uint8_t Get_Main_Stage(void){
	return displayInfo.stage;
}
void Add_Main_Stage(void){
	displayInfo.stage++;
}

void Sub_Main_Stage(void){
	displayInfo.stage--;
}

void Set_Main_Stage(uint8_t temp_n){
	displayInfo.stage = temp_n;
}

void Set_Update_Flag(uint8_t temp_n){
	displayInfo.updateFlag = temp_n;
}

void Set_Main_Count(uint8_t temp_n){
	displayInfo.disCnt500ms = temp_n;
}

void Set_Time_Reload(void){
	displayInfo.time_count = 0;
	if( displayInfo.blinkFlag ){
		displayInfo.disCnt500ms = 1;
	}
}

uint8_t Get_Set_Start_Flag(void){
	return displayInfo.set_start_flag;
}

void Anti_Set_Start_Flag(void){
	displayInfo.set_start_flag = !displayInfo.set_start_flag;
}

void Set_Blink_Flag(uint8_t temp_n){
	displayInfo.blinkFlag = temp_n;
}

/**********************************************************
* @brief 单独显示一段 
* @param  event_type 单独显示的段    mode:E_DIS_ON 显示  E_DIS_OFF：不显示
* @return 
**********************************************************/
void DisplayLcdSegEventSend(e_event_em event_type,e_dismode_t mode)
{
  switch(event_type)
	{
		case ELEM_MEM_SYMB:
		    Dis_Seg_S1(mode);
		break;
		case ELEM_RING_SYMB:
		    Dis_Seg_R1(mode);
		break;	
		case ELEM_NFC_SYMB:
		    Dis_Seg_S2(mode);
		break;
		case ELEM_BLE_SYMB:
		    Dis_Seg_S3(mode);
		break;
		case ELEM_VOICE_SYMB:
		    Dis_Seg_S4(mode);
		break;
		case ELEM_CONTROL_SYMB:
		    Dis_Seg_S5(mode);
		break;	
		case ELEM_WATER_DROP_SYMB:
		    Dis_Seg_S7(mode);
		break;
		case ELEM_S8_SYMB:
		    Dis_Seg_S8(mode);
		break;	
		case ELEM_S9_SYMB:
		    Dis_Seg_S9(mode);
		break;
		case ELEM_S10_SYMB:
		    Dis_Seg_S10(mode);
		break;
		case ELEM_S11_SYMB:
		    Dis_Seg_S11(mode);
		break;
		case ELEM_MMOL_SYMB:
		    Dis_Seg_S12(mode);
		break;
		case ELEM_MGDL_SYMB:
		    Dis_Seg_S13(mode);
		break;
		case ELEM_APPLE_SYMB:
		    Dis_Seg_S14(mode);
		break;	
		case ELEM_S15_SYMB:
		    Dis_Seg_S15(mode);
		break;
		case ELEM_S16_SYMB:
		    Dis_Seg_S16(mode);
		break;
		case ELEM_S17_SYMB:
		    Dis_Seg_S17(mode);
		break;
		case ELEM_BATTERY_SYMB:
		    Dis_Seg_S18(mode);
		break;	
		case ELEM_S19_SYMB:
		    Dis_Seg_S19(mode);
		break;
		case ELEM_S22_SYMB:
		    Dis_Seg_S22(mode);
		break;
		case ELEM_S23_SYMB:
		    Dis_Seg_S23(mode);
		break;
		case ELEM_S24_SYMB:
		    Dis_Seg_S24(mode);
		break;	
		case ELEM_S25_SYMB:
		    Dis_Seg_S25(mode);
		break;	
    default:
    break;			
	}

}	
/**********************************************************
* @brief 显示当前时间
* @param  
* @return 
**********************************************************/
void TimeShow(void){
	DisplayLcdSegEventSend(ELEM_S19_SYMB,E_DIS_ON);
	DisplayLcdSegEventSend(ELEM_S23_SYMB,E_DIS_ON);
	DisplayLcdSegEventSend(ELEM_S22_SYMB,E_DIS_OFF);
	DisPlayNumber(4,1);
	DisPlayNumber(5,0);
	DisPlayNumber(6,0);
	DisPlayNumber(7,9);
	
	DisPlayNumber(8,1);
	DisPlayNumber(9,2);
	DisPlayNumber(10,1);
	DisPlayNumber(11,2);
}

/**********************************************************
* @brief 开机状态的全显与时间显示逻辑
* @param  stage:0-关机 1-全显 2-时间 3-默认  disCnt500ms:计时(0.5s)  updateFlag:计时打开标志位
* @return 
**********************************************************/
void DisplayFrame0(void)
{
	if( displayInfo.stage == 0){
		ClearLcdRam();
	}else if( displayInfo.stage == 1 ){
		FullLcdRam();
		if( displayInfo.updateFlag && (!displayInfo.disCnt500ms) ){
			displayInfo.stage = 2;
			displayInfo.disCnt500ms = 2;
			ClearLcdRam();
		}
	}else if( displayInfo.stage == 2 ){
		TimeShow();
		if( displayInfo.updateFlag && (!displayInfo.disCnt500ms) ){
			ChangeGameStatus(E_LoadGame);
			
			displayInfo.stage = 19;
			displayInfo.updateFlag = 0;
			displayInfo.disCnt500ms = 0;
			ClearLcdRam();
		}
	}
}

/**********************************************************
* @brief 单位显示
* @param  
* @return 
**********************************************************/
void UnitShow(void){
	DisplayLcdSegEventSend(ELEM_MMOL_SYMB,E_DIS_ON);
	DisplayLcdSegEventSend(ELEM_S8_SYMB,E_DIS_ON);
}

/**********************************************************
* @brief 苹果图标显示逻辑
* @param  
* @return 
**********************************************************/
void AppleDisplay(uint8_t apple_status ){
	switch( apple_status ){
		case 0:
			DisplayLcdSegEventSend(ELEM_APPLE_SYMB,E_DIS_OFF);
			DisplayLcdSegEventSend(ELEM_S15_SYMB,E_DIS_OFF);
			DisplayLcdSegEventSend(ELEM_S16_SYMB,E_DIS_ON);
			break;
		
		case 1:
			DisplayLcdSegEventSend(ELEM_APPLE_SYMB,E_DIS_OFF);
			DisplayLcdSegEventSend(ELEM_S15_SYMB,E_DIS_ON);
			DisplayLcdSegEventSend(ELEM_S16_SYMB,E_DIS_OFF);
			break;
		
		case 2:
			DisplayLcdSegEventSend(ELEM_APPLE_SYMB,E_DIS_ON);
			DisplayLcdSegEventSend(ELEM_S15_SYMB,E_DIS_OFF);
			DisplayLcdSegEventSend(ELEM_S16_SYMB,E_DIS_OFF);
			break;
		
		case 3:
			DisplayLcdSegEventSend(ELEM_APPLE_SYMB,E_DIS_OFF);
			DisplayLcdSegEventSend(ELEM_S15_SYMB,E_DIS_OFF);
			DisplayLcdSegEventSend(ELEM_S16_SYMB,E_DIS_OFF);
			break;
		
		default:
			break;
	}
}
/**********************************************************
* @brief 记忆天数显示
* @param  
* @return 
**********************************************************/
void AveDayShow(uint8_t S_day){
	switch(S_day){
		case 1:
			DisPlayNumber(4,28);
			DisPlayNumber(5,28);
			DisPlayNumber(6,0);
			DisPlayNumber(7,1);
			break;
		
		case 7:
			DisPlayNumber(4,28);
			DisPlayNumber(5,28);
			DisPlayNumber(6,0);
			DisPlayNumber(7,7);
			break;
			
		case 14:
			DisPlayNumber(4,28);
			DisPlayNumber(5,28);
			DisPlayNumber(6,1);
			DisPlayNumber(7,4);
			break;
		
		case 30:
			DisPlayNumber(4,28);
			DisPlayNumber(5,28);
			DisPlayNumber(6,3);
			DisPlayNumber(7,0);
			break;
		
		case 90:
			DisPlayNumber(4,28);
			DisPlayNumber(5,28);
			DisPlayNumber(6,9);
			DisPlayNumber(7,0);
			break;
		
		default:
			break;
	}
}
/**********************************************************
* @brief 记忆天数及结果显示逻辑
* @param  
* @return 
**********************************************************/
void AveResultShow(uint8_t T_Result , uint8_t T_count ){
	DisPlayNumber(8 , T_count/100);
	DisPlayNumber(9 ,(T_count%100)/10);
	DisPlayNumber(10,(T_count%100)%10);
	DisPlayNumber(11,27);
	
	DisPlayNumber(1 ,28);
	DisPlayNumber(2 ,T_Result);
	DisPlayNumber(3 ,0);
}
/**********************************************************
* @brief 记忆平均值计算显示逻辑
* @param  
* @return 
**********************************************************/
void AveNumDiaplay(uint8_t T_day , uint8_t T_status ){
	AveDayShow(T_day);
	AveResultShow(9 ,displayInfo.stage );
}
/**********************************************************
* @brief 平均显示逻辑
* @param  
* @return 
**********************************************************/
void AveDisplay(void){
	static const uint8_t day_mark[5] = { 90 , 30 , 14 , 7 , 1 };
	static uint8_t temp_day = 0;
	static uint8_t temp_status = 0;
	temp_status = displayInfo.stage/5;
	temp_day = day_mark[displayInfo.stage%5] ;
	
	AppleDisplay(temp_status);//显示苹果
	AveNumDiaplay(temp_day,temp_status);//显示次数及结果
	DisplayLcdSegEventSend(ELEM_S22_SYMB,E_DIS_ON);//显示日平均
	DisplayLcdSegEventSend(ELEM_S19_SYMB,E_DIS_OFF);
	DisplayLcdSegEventSend(ELEM_S23_SYMB,E_DIS_OFF);
	DisplayLcdSegEventSend(ELEM_S24_SYMB,E_DIS_OFF);
	DisplayLcdSegEventSend(ELEM_S25_SYMB,E_DIS_OFF);
}
/**********************************************************
* @brief 记忆模式显示逻辑
* @param  stage:<20平均值 >20记忆值
* @return 
**********************************************************/

void DisplayFrame1(void)
{	
	DisplayLcdSegEventSend(ELEM_MEM_SYMB,E_DIS_ON);
	if( displayInfo.stage <= 19 ){
		UnitShow();
		AveDisplay();//平均值
	}else{
		//记忆值
	}
	
}

/**********************************************************
* @brief 显示Set
* @param  
* @return 
**********************************************************/
void SetShow(void){
	DisPlayNumber(1 ,26);
	DisPlayNumber(2 ,14);
	DisPlayNumber(3 ,25);
	DisplayLcdSegEventSend(ELEM_S9_SYMB,E_DIS_ON);
	DisplayLcdSegEventSend(ELEM_S10_SYMB,E_DIS_ON);
}

/**********************************************************
* @brief 闪烁显示yes/no
* @param  
* @return 
**********************************************************/
void yesornoBlinkShow(void){
	if( displayInfo.disCnt500ms ){
		if(displayInfo.set_start_flag){
			DisPlayNumber(4 ,4);
			DisPlayNumber(5 ,14);
			DisPlayNumber(6 ,5);
		}else{//no
			DisPlayNumber(4 ,27);
			DisPlayNumber(5 ,18);
			DisPlayNumber(6 ,28);
		}
	}else{
		DisPlayNumber(4 ,28);
		DisPlayNumber(5 ,28);
		DisPlayNumber(6 ,28);
	}
}

/**********************************************************
* @brief 显示BT(暂不做处理)
* @param  
* @return 
**********************************************************/
void ShowBt(void){
	DisPlayNumber(2 ,11);
	DisPlayNumber(3 ,25);
	DisplayLcdSegEventSend(ELEM_S9_SYMB,E_DIS_ON);
	DisplayLcdSegEventSend(ELEM_S10_SYMB,E_DIS_ON);
	
	DisPlayNumber(8 ,10);
	DisPlayNumber(9 ,10);
	DisPlayNumber(10 ,10);
	DisPlayNumber(11 ,10);
}
/**********************************************************
* @brief 设置年份
* @param  
* @return 
**********************************************************/
void ShowYear(void){
	uint16_t temp_year = Get_Memory_Year();
	if( displayInfo.disCnt500ms ){
		DisPlayNumber(2,(temp_year-2000)/10);
		DisPlayNumber(3 ,(temp_year-2000)%10);
		
		DisPlayNumber(4 ,temp_year/1000);
		DisPlayNumber(5 ,(temp_year%1000)/100);
		DisPlayNumber(6 ,((temp_year%1000)%100)/10);
		DisPlayNumber(7 ,((temp_year%1000)%100)%10);
		
	}else{
		DisPlayNumber(2 ,28);
		DisPlayNumber(3 ,28);
		
		DisPlayNumber(4 ,28);
		DisPlayNumber(5 ,28);
		DisPlayNumber(6 ,28);
		DisPlayNumber(7 ,28);
	}
}

/**********************************************************
* @brief 设置月份闪烁 0-月 1-日 2-时间模式设置 3-时 4-分
* @param  
* @return 
**********************************************************/
void ShowSetTime(uint8_t temp_i){
	uint8_t temp_month = Get_Memory_Month();
	uint8_t temp_day   = Get_Memory_Day();
	uint8_t temp_hour = Get_Memory_Hour();
	uint8_t temp_hour_flag = 0;
	uint8_t temp_minute   = Get_Memory_Minute();
	
	if( temp_hour > 12){
		temp_hour_flag = 1;
	}
	
	if( displayInfo.disCnt500ms ){
		
		switch( temp_i ){
			case 0:
				DisPlayNumber(1 ,28);
				DisPlayNumber(2 ,((temp_month<10) ? 0 : 1));
				DisPlayNumber(3 ,temp_month%10);
				break;
			
			case 1:
				DisPlayNumber(1 ,28);
				DisPlayNumber(2 ,((temp_day<10) ? 0 : (temp_day/10)));
				DisPlayNumber(3 ,temp_day%10);
				break;
			
			case 2:
				if( Get_Memory_Time_Set() ){
					DisPlayNumber(1 ,2);
					DisPlayNumber(2 ,4);
					DisPlayNumber(3 ,16);
				}else{
					DisPlayNumber(1 ,1);
					DisPlayNumber(2 ,2);
					DisPlayNumber(3 ,16);
				}
				break;
				
			case 3:
				if( Get_Memory_Time_Set() ){
					DisPlayNumber(1 ,28);
					DisPlayNumber(2 ,((temp_hour<10) ? 0 : (temp_hour/10)));
					DisPlayNumber(3 ,temp_hour%10);
				}else{
					DisPlayNumber(1 ,28);
					DisPlayNumber(2 ,((temp_hour<10) ? 0 : (temp_hour/10)));
					DisPlayNumber(3 ,temp_hour%10);
				}
				break;
			
			case 4:
				DisPlayNumber(1 ,28);
				DisPlayNumber(2 ,((temp_minute<10) ? 0 : (temp_minute/10)));
				DisPlayNumber(3 ,temp_minute%10);	
				break;
		}
		
		DisPlayNumber(4 ,((temp_month<10) ? 0 : 1));
		DisPlayNumber(5 ,temp_month%10);

		DisPlayNumber(6 ,((temp_day<10) ? 0 : (temp_day/10)));
		DisPlayNumber(7 ,temp_day%10);
	
		DisplayLcdSegEventSend(ELEM_S19_SYMB,E_DIS_ON);
	
		if( temp_i == 2 ){
			if(Get_Memory_Time_Set()){
				DisPlayNumber(8 ,2);
				DisPlayNumber(9 ,4);
				DisPlayNumber(10 ,16);
				DisPlayNumber(11 ,28);
				DisplayLcdSegEventSend(ELEM_S23_SYMB,E_DIS_OFF);
				DisplayLcdSegEventSend(ELEM_S24_SYMB,E_DIS_OFF);
				DisplayLcdSegEventSend(ELEM_S25_SYMB,E_DIS_OFF);
			}else{
				DisPlayNumber(8 ,1);
				DisPlayNumber(9 ,2);
				DisPlayNumber(10 ,16);
				DisPlayNumber(11 ,28);
				DisplayLcdSegEventSend(ELEM_S23_SYMB,E_DIS_OFF);
				DisplayLcdSegEventSend(ELEM_S24_SYMB,E_DIS_OFF);
				DisplayLcdSegEventSend(ELEM_S25_SYMB,E_DIS_OFF);
			}
		}else{
			if( Get_Memory_Time_Set() ){//24h
				DisPlayNumber(8 ,((temp_hour < 10) ? 0 : (temp_hour/10)));
				DisPlayNumber(9 ,(temp_hour%10));
				
				DisplayLcdSegEventSend(ELEM_S24_SYMB,E_DIS_OFF);
				DisplayLcdSegEventSend(ELEM_S25_SYMB,E_DIS_OFF);
			}else{
				if( temp_hour_flag ){
					DisPlayNumber(8 ,(((temp_hour-12) < 10) ? 0 : ((temp_hour-12)/10)));
					DisPlayNumber(9 ,((temp_hour-12)%10));
					DisplayLcdSegEventSend(ELEM_S24_SYMB,E_DIS_OFF);
					DisplayLcdSegEventSend(ELEM_S25_SYMB,E_DIS_ON);
				}else{
					DisPlayNumber(8 ,((temp_hour < 10) ? 0 : (temp_hour/10)));
					DisPlayNumber(9 ,(temp_hour%10));
					DisplayLcdSegEventSend(ELEM_S24_SYMB,E_DIS_ON);
					DisplayLcdSegEventSend(ELEM_S25_SYMB,E_DIS_OFF);
				}
			}
			DisPlayNumber(10 ,((temp_minute<10) ? 0 : (temp_minute/10)));
			DisPlayNumber(11 ,(temp_minute%10));
			DisplayLcdSegEventSend(ELEM_S23_SYMB,E_DIS_ON);
		}
	}else{
		switch( temp_i ){
			case 0:
				DisPlayNumber(1 ,28);
				DisPlayNumber(2 ,28);
				DisPlayNumber(3 ,28);
				
				DisPlayNumber(4 ,28);
				DisPlayNumber(5 ,28);
			break;
			
			case 1:
				DisPlayNumber(1 ,28);
				DisPlayNumber(2 ,28);
				DisPlayNumber(3 ,28);
				
				DisPlayNumber(6 ,28);
				DisPlayNumber(7 ,28);
				break;
			
			case 2:
				DisPlayNumber(1 ,28);
				DisPlayNumber(2 ,28);
				DisPlayNumber(3 ,28);
				
				DisPlayNumber(8 ,28);
				DisPlayNumber(9 ,28);
				DisPlayNumber(10 ,28);
				DisPlayNumber(11 ,28);
				DisplayLcdSegEventSend(ELEM_S23_SYMB,E_DIS_OFF);
				DisplayLcdSegEventSend(ELEM_S24_SYMB,E_DIS_OFF);
				DisplayLcdSegEventSend(ELEM_S25_SYMB,E_DIS_OFF);
				break;
			
			case 3:
				DisPlayNumber(1 ,28);
				DisPlayNumber(2 ,28);
				DisPlayNumber(3 ,28);
				
				DisPlayNumber(8 ,28);
				DisPlayNumber(9 ,28);
				break;
			
			case 4:
				DisPlayNumber(1 ,28);
				DisPlayNumber(2 ,28);
				DisPlayNumber(3 ,28);
				
				DisPlayNumber(10 ,28);
				DisPlayNumber(11 ,28);
				break;
		}
	}
	
	
}

/**********************************************************
* @brief 设置音量显示逻辑
* @param  
* @return 
**********************************************************/
void ShowBeep(void){
	if( displayInfo.disCnt500ms ){
		if( Get_Memory_Voice_Set() ){//On
			DisPlayNumber(1 ,28);
			DisPlayNumber(2 ,0);
			DisPlayNumber(3 ,27);
			DisplayLcdSegEventSend(ELEM_VOICE_SYMB,E_DIS_OFF);
		}else{//OFF
			DisPlayNumber(1 ,0);
			DisPlayNumber(2 ,15);
			DisPlayNumber(3 ,15);
			DisplayLcdSegEventSend(ELEM_VOICE_SYMB,E_DIS_ON);
		}
	}else{
		DisPlayNumber(1 ,28);
		DisPlayNumber(2 ,28);
		DisPlayNumber(3 ,28);
	}
	DisPlayNumber(8 ,11);//bEEP
	DisPlayNumber(9 ,14);
	DisPlayNumber(10 ,14);
	DisPlayNumber(11 ,19);
	
	
}
/**********************************************************
* @brief 设置试纸过期显示逻辑
* @param  
* @return 
**********************************************************/
void ShowEP(void){
	if( displayInfo.disCnt500ms ){
		if( Get_Memory_EP_Set() ){//On
			DisPlayNumber(1 ,28);
			DisPlayNumber(2 ,0);
			DisPlayNumber(3 ,27);
		}else{//OFF
			DisPlayNumber(1 ,0);
			DisPlayNumber(2 ,15);
			DisPlayNumber(3 ,15);
		}
	}else{
		DisPlayNumber(1 ,28);
		DisPlayNumber(2 ,28);
		DisPlayNumber(3 ,28);
	}
	DisPlayNumber(6 ,14);//EP
	DisPlayNumber(7 ,19);
	
}
/**********************************************************
* @brief 设置模式显示逻辑
* @param  
* @return 
**********************************************************/
void DisplayFrame2(void)
{
	switch( displayInfo.stage ){
		case 0:
			SetShow();
			yesornoBlinkShow();
			break;
		
		case 1:
			ShowBt();
			break;
		
		case 2:
			ShowYear();
			break;
		
		case 3:
			ShowSetTime(0);
			break;
		
		case 4:
			ShowSetTime(1);
			break;
		
		case 5:
			ShowSetTime(2);
			break;
		
		case 6:
			ShowSetTime(3);
			break;
		
		case 7:
			ShowSetTime(4);
			break;
		
		case 8:
			ShowBeep();
			break;
		
		case 9:
			ShowEP();
			break;
		
		case 10:
			break;
	}
}

void DisplayFrame3(void)
{
	e_event_em temp_i = 0;
	for( ; temp_i < 24 ; ++temp_i){
		DisplayLcdSegEventSend( temp_i , 1);
	}
	
	for(temp_i = 1 ; temp_i<12; ++temp_i){
		DisPlayNumber(temp_i , 8);
	}
}



/**********************************************************
* @brief 初始化显示相关信息
* @param 
* @return 
**********************************************************/
void InitDisplayInfo(void)
{
  memset(&displayInfo,0,sizeof(displayInfo));
}
/**********************************************************
* @brief 注册回调函数入口
* @param 
* @return 
**********************************************************/
//void LcdDisplayCbSet(void (* pFunc)(void))
//{
//   displayInfo.LcdDisplayFuncCb = pFunc;

//}
void LcdDisplayCbSet(Game_Status f_state)
{
	pLed pLedTask[4] = { &DisplayFrame0 , &DisplayFrame1 , &DisplayFrame2 , &DisplayFrame3};
	
	displayInfo.LcdDisplayFuncCb = pLedTask[f_state];
}
/**********************************************************
* @brief LCD处理任务 
* @param 
* @return 
**********************************************************/
void TaskLcd(void)
{
	if( ++displayInfo.time_count > 6 ){
		displayInfo.time_count = 0;
		if( displayInfo.updateFlag && displayInfo.disCnt500ms){
			displayInfo.disCnt500ms--;
		}
		
		if( displayInfo.blinkFlag ){
			displayInfo.disCnt500ms = !displayInfo.disCnt500ms;
		}
	}
	LcdDisplayCbSet(g_SysInfo.mainState);
	
	displayInfo.LcdDisplayFuncCb();
	
  
}