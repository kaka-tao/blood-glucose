#ifndef __APP_ISP_H__
#define __APP_ISP_H__

typedef struct{
	uint16_t Year_Load;
	uint8_t Month_Load;
	uint8_t Day_Load;
	uint8_t Hour_Load;
	uint8_t Minute_Load;
	uint8_t EP_Year_Load;
	uint8_t EP_Month_Load;
	uint16_t Low_Value_Load;
	uint16_t High_Value_Load;
	uint16_t Member_Index_Load;
	
	uint8_t Unit_Load :1;//1:.   2:~.
	uint8_t Time_Set_Load :1;//1:24h  0:12h
	uint8_t Voice_Load :1;
	uint8_t EP_Load :1;
	uint8_t Low_Load :1;
	uint8_t High_Load :1;
	
}Memory_t;

void StampToTime(uint32_t n , Memory_t* clock);
void timToStamp(uint32_t *pStamp, Memory_t clock);
uint16_t Get_Memory_Index(void);
uint16_t Get_Memory_Year(void);
uint8_t Get_Memory_Month(void);
uint8_t Get_Memory_Day(void);
uint8_t Get_Memory_Hour(void);
uint8_t Get_Memory_Minute(void);
uint8_t Get_Memory_Time_Set(void);
uint8_t Get_Memory_Voice_Set(void);
uint8_t Get_Memory_EP_Set(void);

void Sub_Memory_Year(void);
void Add_Memory_Year(void);
void Sub_Memory_Month(void);
void Add_Memory_Month(void);
void Sub_Memory_Day(void);
void Add_Memory_Day(void);
void Sub_Memory_Hour(void);
void Add_Memory_Hour(void);
void Sub_Memory_Minute(void);
void Add_Memory_Minute(void);
void Anti_Memory_Time_Set(void);
void Anti_Memory_Voice_Set(void);
void Anti_Memory_EP_Set(void);

extern void Year_Write(void);
extern void Month_Write(void);
extern void Day_Write(void);
extern void Hour_Set_Write(void);
extern void Hour_Write(void);
extern void Minute_Write(void);
extern void Voice_Set_Write(void);
extern void EP_Set_Write(void);
extern Memory_t MemoryInfo;

void InitMemoryInfo(void);
#endif