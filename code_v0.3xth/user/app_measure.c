#include "main.h"
#include "queue.h"

/**********************************************************
宏
**********************************************************/
#define MAX_BUF_SIZE           30u
#define Measurement_Feedback_Work_R ((float)20.0f)
	
#if 1
	#define Measurement_Base_Voltage_GDH 				((uint16_t)700)
	#define Measurement_Base_Voltage_Ketone 		((uint16_t)1000)
	#define Measurement_Base_Voltage_GOx				((uint16_t)575)
#endif

#if defined (GDH) || defined(DUAL)
code uint8_t voltageList[40] = { 
	   15,15,15,15,15,15,15,15,15,15,
	   15,15,15,15,15,15,15,15,15,15,
	   15,15,15,15,15,15,15,15,15,15,
	   15,15,15,15,15,15,15,15,15,15
};
#elif defined (GOx)
code uint8_t VoltageList[40] = { 
	   18,19,19,18,19,18,19,19,18,11,
	   10,10,10,11,10,10,10,11,12,12,
	   12,12,12,12,12,12,12,12,12,10,
	   10,10, 9,10,10,10, 9,10,10,10
};
#endif
/**********************************************************
全局变量
**********************************************************/
//static Q_Type         g_rcvDataBuf[MAX_BUF_SIZE];
//static QueueType_t    g_rcvQueue;

static NisoSequence_t        g_NisoSequenceStr;
static NisoData_t            g_NisoData;
static RAM_DataTypeDef       RAM_DataStr;

const uint8_t Niso_meaCount[] ={5,10};

static const uint16_t NISO_End[] =
{
#if defined (GDH) || defined (GOx) || defined (DUAL)
	CNT_62_SEC + 1,
	CNT_105_SEC + 10,
#endif	
};

static const float NISO_WorkThreshold[] = 
{
#if defined (GDH) || defined (GOx) || defined (DUAL)
	M_THRESHOLD_WORK,
	M_THRESHOLD_KETONE_WORK,
#endif
};

typedef enum
{
	Adc_Data_Input = 0,
	Adc_Data_Output,
	Adc_Data_Type_Max
}Adc_DataTypeDef;

/**********************************************************
* @brief adc中断处理
* @param 
* @return 
**********************************************************/
void AdcIntCallBack(void)
{
	uint16_t  adcValue;
	
  adcValue = ReadAdcSingleByInt();
	g_NisoData.nisoAdcValue[g_NisoData.AdcCount] = adcValue;
	
	switch(g_NisoData.AdcCount)
	{
		case 2:
			AdcConnectWorkIn();
		break;
		case 4:
			AdcConnect_Opa2_ThirdOut();
		break;
		case 6:
			AdcConnectThirdIn();
		break;
		case 8:
			AdcConnect_Opa1_WorkOut();
		break;
		default:	break;
	}
	
	if(++g_NisoData.AdcCount >= 8)
	{
		g_NisoData.AdcCount = 0;
		g_NisoData.completeFlg = TRUE;
		AdcDisableIsr();//关闭adc中断
	}
	else
	{
    //AdcEnableIsr();
	}
}
/**********************************************************
* @brief 2.5ms定时
* @param 
* @return 
**********************************************************/
void Timer2_5msCallBack(void)
{
 if(g_SysInfo.state == E_Ready_Measure || g_SysInfo.state == E_Measure_Start)
 {
   if(g_NisoData.startFlg == FALSE)
	 {
//		g_NisoData.AdcCount = 0;
		g_NisoData.DacCount++;
		//AdcEnableIsr();
		g_NisoData.startFlg = TRUE;
	 }
 	 else
	 {
		    
	 }
 }
}

/**********************************************************
* @brief 错误处理
* @param 
* @return 
**********************************************************/
void ProSysErr(const SysInfo_t *pInfo)
{
    if(pInfo->errKind == Error_no)  return;
	  
	  switch(pInfo->errKind)
		{
			case Error_1_Used_Strip:
			{
			   DacSetRefWork(0);
				 DacSetRefThird(0);
			}
			break;
			case Error_2_Not_Ready:
			{
			   DacSetRefWork(0);
				 DacSetRefThird(0);
			}
			break;
		  case Error_3_Invalid_Temperature:
			{
			   DacSetRefWork(0);
				 DacSetRefThird(0);
			}
			break;
		  case Error_4_Not_Sufficient_Blood:
			{
			   DacSetRefWork(0);
				 DacSetRefThird(0);
			}
			break;
		}
	  // add other init
}

/**********************************************************
* @brief 处理试纸插入后的温度校准
* @param 
* @return 
**********************************************************/
void Pro_DetInput(void)
{
	uint16_t   temp,baseVoltage,dacWork,ref;
	uint8_t    cnt;
	
  g_SysInfo.realTemp = ReadTempSensor();
  if(g_SysInfo.realTemp > 450)      temp = 45;
	else if(g_SysInfo.realTemp < 50)  temp = 5;
	else                              temp = (uint16_t)((float)g_SysInfo.realTemp/10.0f+0.5f);
	
#if defined (GOx)
	baseVoltage =	Measurement_Base_Voltage_GOx;
#elif defined (GDH)  
	baseVoltage =	Measurement_Base_Voltage_GDH;
#endif
	for(cnt = 0;cnt < (temp - 5);cnt++){
	    baseVoltage -= voltageList[cnt];
	}

	g_NisoSequenceStr.thirdDetectionTime = CNT_17_SEC;
	
  ref = GetNref();
  dacWork = (uint16_t)(((float)baseVoltage / ((float)ref/ 40950.0f)) * ((float)ref/20000.0f) +0.5f);

	DacSetRefWork(dacWork);
	
	ChangeMainState(E_ValidationStrip);
}
/**********************************************************
* @brief 测量前的准备
* @param 
* @return 
**********************************************************/
static void PreMeasure(flashCalib_t *pCalib)
{
  //QueueInit(&g_rcvQueue, g_rcvDataBuf, MAX_BUF_SIZE);
  
	g_NisoSequenceStr.RawDataCount = 0;
	g_NisoSequenceStr.workConfirmCount = 0;
	g_NisoSequenceStr.thirdConfirmCount = 0;
	g_NisoSequenceStr.thirdConfirm = FALSE;
	g_NisoSequenceStr.Type = Measurement_Glucose;
	g_NisoSequenceStr.measurementCount = Niso_meaCount[g_NisoSequenceStr.Type];
	
  AdcConnect_Opa1_WorkOut();//ADC连接到WorkOut
	SetDacValue(pCalib->dacCalWork);

	g_NisoData.startFlg = FALSE;
	g_NisoData.completeFlg = FALSE;
	
	Enable_2_5msTimer();
}
/**********************************************************
* @brief 测量前对试纸条进行检测
* @param 
* @return 
**********************************************************/
void MeasurementValidateStrip(SysInfo_t *pInfo)
{
  static uint32_t lastSysCnts = 0;
	uint32_t newSysCnts = 0;
	static uint8_t  count_100ms = 0;
	float inCurrent,outCurrent,adcCurrent;
	uint16_t nAREF;
  
	newSysCnts = GetGlobalCount();
	if((newSysCnts-lastSysCnts) >= 100){
	   count_100ms++;
		 lastSysCnts = newSysCnts;
		
		 if(1 == count_100ms)
		 {
       if(pInfo->realTemp < TEMP_LOW || pInfo->realTemp > TEMP_HIGH)
			 {
				 pInfo->state = E_ProError;
				 
				 lastSysCnts = 0;
			   newSysCnts= 0;
				 count_100ms = 0;
				 
				 pInfo->errKind = Error_3_Invalid_Temperature;
				 
				 return;
			 }
		 }
		 else if(2 == count_100ms)
		 {
			 nAREF = GetNref();
			 inCurrent = SenseAdAverage(CH_WORK_IN,15);
			 outCurrent = SenseAdAverage(CH_WORK_OUT,15);
			 adcCurrent = (outCurrent - inCurrent)/ 40950.0f * \
			              ((float)nAREF/M_FEEDBACK_WORK_R * 100.0f);
			 if(adcCurrent > M_THRESHOLD_ERR_1 || outCurrent == 0xffff || inCurrent == 0xffff)										 
			 {
				 pInfo->state = E_ProError;
				 
				 lastSysCnts = 0;
			   newSysCnts= 0;
				 count_100ms = 0;
				 
				 pInfo->errKind = Error_1_Used_Strip;
				 return;
			 }							 
		   
		 }
		 else if((18 <= count_100ms) && (count_100ms <= 21))  
		 {
			 nAREF = GetNref();
			 inCurrent = SenseAdAverage(CH_WORK_IN,15);
			 outCurrent = SenseAdAverage(CH_WORK_OUT,15);
			 adcCurrent = (outCurrent - inCurrent)/ 40950.0f * \
			              (float)nAREF/M_FEEDBACK_WORK_R * 100.0f;
			 if(adcCurrent > M_THRESHOLD_WORK || outCurrent == 0xffff || inCurrent == 0xffff)										 
			 {
				 pInfo->state = E_ProError;
				 
				 lastSysCnts = 0;
			   newSysCnts= 0;
				 count_100ms = 0;
				 
				 pInfo->errKind = Error_2_Not_Ready;
				 return;
			 }							 
		 }
		 
		 if(21 == count_100ms)
		 {
			 lastSysCnts = 0;
			 newSysCnts= 0;
			 count_100ms = 0;
			 
			 PreMeasure(&g_calData);
			 pInfo->state = E_Ready_Measure;		   
			 
		 }
			 
 		 if(8 == count_100ms || 16 == count_100ms){
			 StripSet(io_code_1);
		 }
		 if(9 == count_100ms || 17 == count_100ms){
			 StripRead(io_code_1, &g_CurrentS.Code);
			 StripSet(io_code_2);
		 }
		 if(10 == count_100ms || 18 == count_100ms){
			 StripRead(io_code_2, &g_CurrentS.Code);
			 StripSet(io_code_3);
		 }
		 if(11 == count_100ms || 19 == count_100ms){
			 StripRead(io_code_3, &g_CurrentS.Code);
			 StripSet(io_code_4);
		 }	
		 if(12 == count_100ms || 20 == count_100ms){
		   StripRead(io_code_4, &g_CurrentS.Code);
			 StripSet(io_code_all);
			 if(count_100ms == 12)
			 {
				 VerifyStripCode(g_CurrentS.Code, Strip_Verify_1);
			 }
			 else
			 {
				  VerifyStripCode(g_CurrentS.Code, Strip_Verify_2);
				
				  if(Is_Strip_Available(&g_CurrentS.Code) == FALSE)
				  {
					  //if((g_CurrentS.Flag_DisplayDebugMessage != Jig_Mode) || (g_CurrentS.Code == 0xff)) // JIG Mode 			
            if(g_CurrentS.Code == 0xff)							
					  {
						  pInfo->state = E_ProError;
						 
						  lastSysCnts = 0;
						  newSysCnts= 0;
						  count_100ms = 0;
						 
						  pInfo->errKind = Error_5_Invalid_Strip_Code;
						  //GPIO_Init(Strip_Detection_GPIO, Strip_Detection_Pin, GPIO_Mode_In_PU_No_IT);
						  return;
					  }
				  }
			  }
		 }		 
	}

}

static void SetRefPwm(void)
{
	if(g_NisoData.DacCount >= 0 && g_NisoData.DacCount < CNT_20_SEC)
	{
			//add hard drv code
			//workRef一直输出0
			if(g_NisoData.DacCount % 2 == 0)
			{
				 //thirdRef输出200mv DacSetRefThird(g_calData.dacCalThird);
				 DaoConnectOp2p();
			}
			else
			{
				 //thirdRef输出0
				 DaoDisConnectOp();
			}
	}
	//else if(g_NisoData.DacCount >= CNT_20_SEC && g_NisoData.DacCount < CNT_50_SEC)
	else if(g_NisoData.DacCount == CNT_20_SEC)
	{
			//work一直输出200mv   third一直输出0
			DaoConnectOp1p();
	}
	else if(g_NisoData.DacCount >= CNT_50_SEC && g_NisoData.DacCount < CNT_62_SEC)
	{
			//add hard drv code
			//work一直输出0
			if(g_NisoData.DacCount % 2 == 0)
			{
				 //third输出200mv   DacSetRefThird(g_calData.dacCalThird);
				 DaoConnectOp2p();
			}
			else
			{
				 //third输出0
				 DaoDisConnectOp();
			}
	}
}
/**********************************************************
* @brief 对采样到原始信号进行初步处理
* @param 
* @return 
**********************************************************/
static void CalculateAdcData(NisoData_t *pData)
{
	int16_t  adcWork[Adc_Data_Type_Max];
	int16_t  adcThird[Adc_Data_Type_Max];
	
	adcWork[Adc_Data_Output] = (pData->nisoAdcValue[0] + pData->nisoAdcValue[1])/2;
	adcWork[Adc_Data_Input] = (pData->nisoAdcValue[2] + pData->nisoAdcValue[3])/2;
	pData->workResult = adcWork[Adc_Data_Output] - adcWork[Adc_Data_Input];
	
	adcThird[Adc_Data_Output] = (pData->nisoAdcValue[4] + pData->nisoAdcValue[5])/2;
	//adcThird[Adc_Data_Input] = (pData->nisoAdcValue[6] + pData->nisoAdcValue[7])/2;
	//pData->thirdResult = adcThird[Adc_Data_Output] - adcThird[Adc_Data_Input];
  pData->thirdResult = adcThird[Adc_Data_Output];	
}
/**********************************************************
* @brief  信号检波主流程
* @param  系统状态
* @return 
**********************************************************/
void NisoMeasurement(SysInfo_t *pInfo)
{
	 float    adcStart = 0;
	 float    ref;
	 uint16_t transData;
	 uint8_t  kkkError = 0 ;
	 uint16_t ketoneUiOffset;
	 
	 if(pInfo->state != E_Ready_Measure && pInfo->state != E_Measure_Start)  return;
	 if(FALSE == g_NisoData.startFlg)          return;
		
	 g_NisoData.startFlg = FALSE;
	
	 
	 if(pInfo->state == E_Ready_Measure)//检测血液是否滴入
	 {
//		 adcStart = (float)g_NisoData.workResult;
		 adcStart = (float)(ReadAdcSingle() - g_calData.adcRefValue);
		 ref = (float)GetNref();
		 adcStart = adcStart / 40950.0f * ref / M_FEEDBACK_WORK_R * 100.0f;
//////////////////////////////	 
		 g_NisoData.DacCount = 0;
		 pInfo->state = E_Measure_Start;
		 ketoneUiOffset = (g_NisoSequenceStr.Type == Measurement_Ketone) ? 300 : 0 ;
		 return;
//////////////////////////////		
		 //if(adcStart > M_THRESHOLD_THIRD)	//检测到血液滴入
		 if(adcStart > NISO_WorkThreshold[g_NisoSequenceStr.Type])//19.0f->190nA
		 {
			 if(++g_NisoSequenceStr.workConfirmCount >= 8)	//检测到血液滴入
			 {
				 // add hard drv code
				 g_NisoData.DacCount = 0;
				 
				 pInfo->state = E_Measure_Start;
				 g_NisoSequenceStr.thirdConfirm = TRUE;//FALSE;
				 g_NisoSequenceStr.thirdCheckDisable = TRUE;//FALSE;
				 
				 ketoneUiOffset = (g_NisoSequenceStr.Type == Measurement_Ketone) ? 300 : 0 ;
			 }
			 else
			 {
				 g_NisoData.DacCount--;
			 
			 }
		 }
		 else
		 {
				g_NisoSequenceStr.workConfirmCount = 0;
				if(g_NisoData.DacCount > 0)		g_NisoData.DacCount--; 		
		 }
	 }
	 
#if 0	 
	 if(g_NisoSequenceStr.thirdConfirm == FALSE && g_NisoSequenceStr.workConfirmCount == 8)
	 {
		 if(g_NisoData.DacCount % 2 == 0){
				g_NisoSequenceStr.thirdAdcFirst = g_NisoData.thirdResult;
		 }
	   else
		 {
		    g_NisoSequenceStr.thirdAdcSecond = g_NisoData.thirdResult;
		    
		    adcStart = g_NisoSequenceStr.thirdAdcSecond - g_NisoSequenceStr.thirdAdcFirst;
        if(adcStart < 0){
				   adcStart = adcStart * (-1.0f);
				}
        if(g_NisoSequenceStr.thirdCheckDisable == FALSE)
        {
					if((adcStart >= (float)M_THRESHOLD_THIRD_ADC) && 
						(g_NisoData.DacCount <= g_NisoSequenceStr.thirdDetectionTime))
					{
					   if(++g_NisoSequenceStr.thirdConfirmCount >= 8)
						 {
						    g_NisoSequenceStr.thirdConfirm = TRUE;
						    
						    g_NisoSequenceStr.workConfirmCount = 0;
							  g_NisoSequenceStr.thirdConfirmCount = 0;
						    // disable....
						 }
					}
				  else if(g_NisoData.DacCount > g_NisoSequenceStr.thirdDetectionTime)
				  {
					    //NISO_ResetMeasurement();
						  pInfo->state = E_ProError;
							pInfo->errKind = Error_1_Used_Strip;
				      return;
					}
					else
					{
					   if(g_NisoSequenceStr.thirdConfirmCount > 0)
					      g_NisoSequenceStr.thirdConfirmCount--;
					}
				}					
		 }
	 }
#endif	 
	 if(pInfo->state == E_Measure_Start)
	 { 
		 if(RAM_DataStr.GlucoseRawData[800] > RAM_DataStr.GlucoseRawData[400])
		 {
			 kkkError = 1;
		 }
#if 0	
		 if(g_NisoData.DacCount > 1 && g_NisoData.DacCount <= CNT_20_SEC) 
     {                                          
       transData += 10000;
		 }
     else if(g_NisoData.DacCount >	CNT_20_SEC && g_NisoData.DacCount < CNT_50_SEC)
		 {				     
			 transData += 0;      
     }
     else if(g_NisoData.DacCount >=	CNT_50_SEC && g_NisoData.DacCount < CNT_62_SEC) 
     {  
			 transData += 10000;
     }
#endif	
		 /***************************************/
     if(g_NisoData.DacCount >=	CNT_05_SEC && g_NisoData.DacCount < CNT_10_SEC)
		 {
			 RAM_DataStr.GlucoseRawData[g_NisoSequenceStr.RawDataCount++] = ReadAdcSingle();
     }                        
     else if(g_NisoData.DacCount >=	CNT_30_SEC && g_NisoData.DacCount < CNT_50_SEC)
		 { 
			 RAM_DataStr.GlucoseRawData[g_NisoSequenceStr.RawDataCount++] = ReadAdcSingle() - g_calData.adcRefValue;
     }   
     else if(g_NisoData.DacCount >=	CNT_55_SEC && g_NisoData.DacCount < CNT_60_SEC)
		 {
			 RAM_DataStr.GlucoseRawData[g_NisoSequenceStr.RawDataCount++] = ReadAdcSingle();
     }

     SetRefPwm();		 
     
     //flash 相关代码		 
	   if(g_NisoData.DacCount > NISO_End[g_NisoSequenceStr.Type] + ketoneUiOffset) 
		 {
			  g_NisoData.DacCount = 0;
		    Disable_2_5msTimer();//关闭定时器
			 
			  if(1 ==kkkError)
				{
					pInfo->state = E_ProError;
					pInfo->errKind = Error_4_Not_Sufficient_Blood;
				}
				else
				{
				  pInfo->state = E_Measure_Confirm;
				}
		 }
		 //Lcd相关代码
	 }
}

void MeasurementConfirmResult(SysInfo_t *pInfo)
{
	uint16_t glucose_value =0;
	uint16_t ret;

//	ret = NISO_isens630(GetNref(), M_FEEDBACK_THIRD_R, M_FEEDBACK_WORK_R, pInfo->realTemp,  &(RAM_DataStr.GlucoseRawData[0]), 
//							g_CurrentS.Code, g_CurrentR.GlucoseFlag.FlagCS,&glucose_value);
							
	if(ret == Error_4_Not_Sufficient_Blood + 1)
	{
			//NISO_ResetMeasurement();
		  pInfo->state = E_ProError;
			pInfo->errKind = Error_4_Not_Sufficient_Blood;
			return; 
	}
}
#if 0
void NisoMeasurement(SysInfo_t *pInfo)
{
	float      adcStart = 0;
	uint16_t   transData;
	uint8_t    error = 0 ; 

	uint8_t    search_cnt;
	//0-20  work_out    20-40   work_in  40-44  Third_out   44-48   Third_in
 	if(pInfo->state != E_Measure_Start)
		return; 
	
	if(FALSE == g_NisoData.startFlg)          return;
	if(FALSE == g_NisoData.completeFlg)       return;
		
	g_NisoData.startFlg = FALSE;
	g_NisoData.completeFlg = FALSE;	
		      
}
#endif