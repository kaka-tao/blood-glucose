/***************************************
* bref 该文件进行flash操作
***************************************/
#include "main.h"


flashCalib_t g_calData;	
FlashStorage_BGM_InfoTypeDef g_CurrentS;
FlashStorage_GlucoseResult_TypeDef g_CurrentR;

void InitCalibData(void)
{
//	uint16_t  CalValue = 0;
//	g_calData.dacCalThird = 409;
//	g_calData.dacCalWork = 409;
	
	g_CurrentS.Code = 0;
	
//	CalValue = CalibDaoValue();
	
//	g_calData.dacCalThird = CalValue;
//	g_calData.dacCalWork = CalValue;

#if CONTROL_SOLUTION	
	g_CurrentR.GlucoseFlag.FlagCS = 1;
#else
	g_CurrentR.GlucoseFlag.FlagCS = 0;
#endif	
}
void SetNref(uint16_t value)
{
   g_calData.nAREF_mV = value;
}
uint16_t GetNref(void)
{
	if(g_calData.nAREF_mV <= 17000||g_calData.nAREF_mV >= 23000){
		g_calData.nAREF_mV = 20000; 
	}
  return g_calData.nAREF_mV;
}

void CalibDaoValue(void)
{
  static uint32_t lastSysCnts = 0;
	uint32_t newSysCnts = 0;
	uint16_t value;
	
	newSysCnts = GetGlobalCount();
	if((newSysCnts - lastSysCnts) >= 5)
	{
		 lastSysCnts = newSysCnts;
	   value = ReadAdcSingle();
		 if(value > 410){
        g_calData.dacCalWork --;
        SetDacValue(g_calData.dacCalWork);		
		 }
     else if(value < 408){
        g_calData.dacCalWork ++;
        SetDacValue(g_calData.dacCalWork);		
		 }		 
		 else
     {
			  g_calData.adcRefValue = value;
			  g_calData.dacCalThird = g_calData.dacCalWork;
		    g_SysInfo.state = E_StartWait;
		 }			   
	}
}
 