#include "main.h"
#include "bsp_isp.h"

#define SET_MIN_YEAR            2023
#define SET_MAX_YEAR			2050

Memory_t MemoryInfo;

static  uint8_t MON1[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};	//平年
static  uint8_t MON2[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};	//闰年
void ISP_delay_ms(uint16_t n){
	LCD_delay_ms(n);
}

uint16_t Get_Memory_Index(void){
	return MemoryInfo.Member_Index_Load;
}

uint16_t Get_Memory_Year(void){
	return MemoryInfo.Year_Load;
}
uint8_t Get_Memory_Month(void){
	return MemoryInfo.Month_Load;
}
uint8_t Get_Memory_Day(void){
	return MemoryInfo.Day_Load;
}
uint8_t Get_Memory_Hour(void){
	return MemoryInfo.Hour_Load;
}
uint8_t Get_Memory_Minute(void){
	return MemoryInfo.Minute_Load;
}
uint8_t Get_Memory_Time_Set(void){
	return MemoryInfo.Time_Set_Load;
}
uint8_t Get_Memory_Voice_Set(void){
	return MemoryInfo.Voice_Load;
}
uint8_t Get_Memory_EP_Set(void){
	return MemoryInfo.EP_Load;
}

void Sub_Memory_Year(void){
	if(MemoryInfo.Year_Load > SET_MIN_YEAR){
		MemoryInfo.Year_Load--;
	}else{
		MemoryInfo.Year_Load = SET_MAX_YEAR;
	}
}

void Add_Memory_Year(void){
	if( MemoryInfo.Year_Load < SET_MAX_YEAR){
		MemoryInfo.Year_Load++;
	}else{
		MemoryInfo.Year_Load = SET_MIN_YEAR;
	}
}

void Sub_Memory_Month(void){
	uint8_t run_year;
	uint8_t run_day;
	if( (( (MemoryInfo.Year_Load%4 == 0)&&(MemoryInfo.Year_Load%100 != 0) ) || (MemoryInfo.Year_Load%400 == 0 ))){
		run_year = 1;
	}else{
		run_year = 0;
	}
	if( MemoryInfo.Month_Load > 1 ){
		MemoryInfo.Month_Load--;
	}else{
		MemoryInfo.Month_Load = 12;
	}
	if( run_year ){
		run_day = MON2[MemoryInfo.Month_Load-1];
	}else{
		run_day = MON1[MemoryInfo.Month_Load-1];
	}
	
	if( MemoryInfo.Day_Load > run_day ){
		MemoryInfo.Day_Load = run_day;
	}
}

void Add_Memory_Month(void){
	uint8_t run_year;
	uint8_t run_day;
	if( (( (MemoryInfo.Year_Load%4 == 0)&&(MemoryInfo.Year_Load%100 != 0) ) || (MemoryInfo.Year_Load%400 == 0 ))){
		run_year = 1;
	}else{
		run_year = 0;
	}
	if( MemoryInfo.Month_Load < 12 ){
		MemoryInfo.Month_Load++;
	}else{
		MemoryInfo.Month_Load = 1;
	}
	if( run_year ){
		run_day = MON2[MemoryInfo.Month_Load-1];
	}else{
		run_day = MON1[MemoryInfo.Month_Load-1];
	}
	
	if( MemoryInfo.Day_Load > run_day ){
		MemoryInfo.Day_Load = run_day;
	}
}

void Sub_Memory_Day(void){
	uint8_t run_year;
	uint8_t run_day;
	if( (( (MemoryInfo.Year_Load%4 == 0)&&(MemoryInfo.Year_Load%100 != 0) ) || (MemoryInfo.Year_Load%400 == 0 ))){
		run_year = 1;
	}else{
		run_year = 0;
	}
	
	if( run_year ){
		run_day = MON2[MemoryInfo.Month_Load-1];
	}else{
		run_day = MON1[MemoryInfo.Month_Load-1];
	}
	
	if( MemoryInfo.Day_Load > 1 ){
		MemoryInfo.Day_Load--;
	}else{
		MemoryInfo.Day_Load = run_day;
	}
}
void Add_Memory_Day(void){
	uint8_t run_year;
	uint8_t run_day;
	if( (( (MemoryInfo.Year_Load%4 == 0)&&(MemoryInfo.Year_Load%100 != 0) ) || (MemoryInfo.Year_Load%400 == 0 ))){
		run_year = 1;
	}else{
		run_year = 0;
	}
	
	if( run_year ){
		run_day = MON2[MemoryInfo.Month_Load-1];
	}else{
		run_day = MON1[MemoryInfo.Month_Load-1];
	}
	
	if( MemoryInfo.Day_Load < run_day ){
		MemoryInfo.Day_Load++;
	}else{
		MemoryInfo.Day_Load = 1;
	}
}

void Sub_Memory_Hour(void){
	if( MemoryInfo.Hour_Load > 1){
		MemoryInfo.Hour_Load--;
	}else{
		MemoryInfo.Hour_Load = 24;
	}
}

void Add_Memory_Hour(void){
	if( MemoryInfo.Hour_Load < 24){
		MemoryInfo.Hour_Load++;
	}else{
		MemoryInfo.Hour_Load = 1;
	}
}
void Sub_Memory_Minute(void){
	if( MemoryInfo.Minute_Load > 0){
		MemoryInfo.Minute_Load--;
	}else{
		MemoryInfo.Minute_Load = 59;
	}
}

void Add_Memory_Minute(void){
	if( MemoryInfo.Minute_Load < 59){
		MemoryInfo.Minute_Load++;
	}else{
		MemoryInfo.Minute_Load = 0;
	}
}
void Anti_Memory_Time_Set(void){
	MemoryInfo.Time_Set_Load = !MemoryInfo.Time_Set_Load;
}
void Anti_Memory_Voice_Set(void){
	MemoryInfo.Voice_Load = !MemoryInfo.Voice_Load;
}
void Anti_Memory_EP_Set(void){
	MemoryInfo.EP_Load = !MemoryInfo.EP_Load;
}
/************************************************
*  获取bit位的01
*************************************************/
uint8_t get_bit(uint8_t number , uint8_t sort ){
	uint8_t temp_i = 1;
	return ((number>>sort)&temp_i);
}
/************************************************
*  设置bit位的1
*************************************************/
uint8_t set_bit1(uint8_t number , uint8_t sort){
	uint8_t temp_i = 1;
	return number |= (temp_i<<sort);
}
/************************************************
*  设置bit位的0
*************************************************/
uint8_t set_bit0(uint8_t number , uint8_t sort){
	uint8_t temp_i = 1;
	return number &= ~(temp_i<<sort);
}
/************************************************
*  时间转时间戳  uint32_t
*************************************************/
void timToStamp(uint32_t *pStamp, Memory_t clock)
{
	uint8_t *month = NULL;
	uint8_t leapYearCnt = 0;
	uint16_t days = 0;
	uint16_t i = 0;
	//获得1970年到当前年的前一年共有多少闰天
	for( i = 1970; i < clock.Year_Load; i++)
	{
		if((i % 4 == 0 && i % 100 != 0) || (i % 400 == 0))
		{
			leapYearCnt++;
		}
	}
	days = leapYearCnt * 366 + (clock.Year_Load - 1970 -leapYearCnt) * 365;
	/*判断当前年是不是闰年*/
	if((clock.Year_Load % 4 == 0 && clock.Year_Load % 100 != 0) || (clock.Year_Load % 400 == 0))
	{
		month = MON2;
	}
	else
	{
		month = MON1;
	}
	
	for(i = 0; i < clock.Month_Load - 1; i++)
	{
		days += month[i];
	}
	
	*pStamp = (days + clock.Day_Load-1) * 24 * 3600 * 1000 + clock.Hour_Load * 3600 * 1000 + clock.Minute_Load * 60 * 1000 ;
}

/************************************************
*  时间戳转时间
*************************************************/
void StampToTime(uint32_t n , Memory_t* clock)
{
	uint32_t days = 0;
	uint32_t rem = 0;
	uint16_t year = 0;
	uint16_t leap = 0;
	uint16_t ydays = 0;
	uint16_t month = 0;
	uint16_t mdays = 0;
	//static const uint16_t days_month[12] = {31 , 28 , 31 , 30 , 31 , 30 ,31,31,30,31,30,31 };
	days = (uint32_t)( n / 86400 );
	rem = (uint32_t)( n % 86400 );
	for( year = 1970 ; ; ++year ){
		leap = (( (year%4 == 0) && (year%100 != 0) ) || (year%400 == 0));
		ydays = leap ? 366 : 365 ;
		if( days < ydays ){
			break;
		}
		days -= ydays ;
	}
	clock->Year_Load = year;
    
	for( month = 0 ; month < 12 ; ++month ){
		mdays = MON1[month];
		if( (month == 1) && (( (year%4 == 0)&&(year%100 != 0) ) || (year%400 == 0 ))){
			mdays = 29;
		}
		if( days < mdays ){
			break;
		}
		days -= mdays;
	}
	clock->Month_Load = month;
	clock->Month_Load += 1;
	
	clock->Day_Load = days+1;
	
	clock->Hour_Load = rem / 3600;
	rem %= 3600;
	clock->Minute_Load = rem / 60;
}

/*************************************************************
定义存储结构
    MEMORY_TAB[0]   血糖[低位]
    MEMORY_TAB[1]   血糖[高位]
    MEMORY_TAB[2]   时间戳    
    MEMORY_TAB[3]   
    MEMORY_TAB[4]   
    MEMORY_TAB[5]   
	MEMORY_TAB[6]   苹果类型
	MEMORY_TAB[7]   暂留
**************************************************************/

uint8_t idata MEMORY_TAB[8] = {0,0,0,0,0,0,0,0};
uint32_t xdata MEM_Addr;
/**********************************************************
* @brief 初始化记忆相关信息并读取
* @param 
* @return 
**********************************************************/
void InitMemoryInfo(void){
	memset(&MemoryInfo,0,sizeof(MemoryInfo));
	DisableIrq();
	ISP_delay_ms(10); 
	
	if( (GetRomYear() <SET_MIN_YEAR) || (GetRomYear() > SET_MAX_YEAR) ){
		MemoryInfo.Year_Load = 2023;
		MemoryInfo.Month_Load = 10;
		MemoryInfo.Day_Load = 1;
		MemoryInfo.Hour_Load = 12;
		MemoryInfo.Minute_Load = 31;
		MemoryInfo.EP_Year_Load = 0;
		MemoryInfo.EP_Month_Load = 0;
		MemoryInfo.Low_Value_Load = 0;
		MemoryInfo.High_Value_Load = 0;
		MemoryInfo.Member_Index_Load = 0;
		
		MemoryInfo.Unit_Load = 1;
		MemoryInfo.Time_Set_Load = 1;
		MemoryInfo.Voice_Load = 1;
		MemoryInfo.EP_Load = 0;
		MemoryInfo.Low_Load = 0;
		MemoryInfo.High_Load = 0;
	}else{
		MemoryInfo.Year_Load = GetRomYear();
		MemoryInfo.Month_Load = GetRomMonth();
		MemoryInfo.Day_Load = GetRomDay();
		MemoryInfo.Hour_Load = GetRomHour();
		MemoryInfo.Minute_Load = GetRomMinute();
		MemoryInfo.EP_Year_Load = GetRomEPYear();
		MemoryInfo.EP_Month_Load = GetRomEPMonth();
		MemoryInfo.Low_Value_Load = GetRomLowValue();
		MemoryInfo.High_Value_Load = GetRomHighValue();
		MemoryInfo.Member_Index_Load = GetRomMemberIndex();
		
		MemoryInfo.Unit_Load = get_bit(GetRomOther() , 0);
		MemoryInfo.Time_Set_Load = get_bit(GetRomOther() , 1);
		MemoryInfo.Voice_Load = get_bit(GetRomOther() , 2);
		MemoryInfo.EP_Load = get_bit(GetRomOther() , 3);
		MemoryInfo.Low_Load = get_bit(GetRomOther() , 4);
		MemoryInfo.High_Load = get_bit(GetRomOther() , 5);
	}
	ISP_delay_ms(10);
	EnableIrq();
}

/**********************************************************
* @brief 写入年份
* @param 
* @return 
**********************************************************/
void Year_Write(void){
    DisableIrq();
    ISP_delay_ms(1);
    ISPWrite(0xFF60 , (uint8_t)MemoryInfo.Year_Load);
    ISPWrite(0xFF61 , (uint8_t)(MemoryInfo.Year_Load>>8));
    ISP_delay_ms(1);
    EnableIrq();
}

/**********************************************************
* @brief 写入月份
* @param 
* @return 
**********************************************************/
void Month_Write(void){
	DisableIrq();
    ISP_delay_ms(1);
	
    ISPWrite(0xFF62 , MemoryInfo.Month_Load);
        
    ISP_delay_ms(1);
    EnableIrq();
}
/**********************************************************
* @brief 写入日
* @param 
* @return 
**********************************************************/
void Day_Write(void){
	DisableIrq();
    ISP_delay_ms(1);
	
    ISPWrite(0xFF63 , MemoryInfo.Day_Load);
        
    ISP_delay_ms(1);
    EnableIrq();
}
/**********************************************************
* @brief 写入时间显示格式
* @param 
* @return 
**********************************************************/
void Hour_Set_Write(void){
	DisableIrq();
    ISP_delay_ms(1);
	memset( &MEMORY_TAB , 0 , sizeof( MEMORY_TAB));
	
	if( MemoryInfo.Time_Set_Load ){
		MEMORY_TAB[0] = set_bit1( GetRomOther() , 1 );
	}else{
		MEMORY_TAB[0] = set_bit0( GetRomOther() , 1 );
	}
    
    ISPWrite(0xFF6E , MEMORY_TAB[0]);
        
    ISP_delay_ms(1);
    EnableIrq();
}
/**********************************************************
* @brief 写入时
* @param 
* @return 
**********************************************************/
void Hour_Write(void){
	DisableIrq();
    ISP_delay_ms(1);
	
    ISPWrite(0xFF64 , MemoryInfo.Hour_Load);
        
    ISP_delay_ms(1);
    EnableIrq();
}

/**********************************************************
* @brief 写入分
* @param 
* @return 
**********************************************************/
void Minute_Write(void){
	DisableIrq();
    ISP_delay_ms(1);
	
    ISPWrite(0xFF65 , MemoryInfo.Minute_Load);
        
    ISP_delay_ms(1);
    EnableIrq();
}
/**********************************************************
* @brief 写入声音设置
* @param 
* @return 
**********************************************************/
void Voice_Set_Write(void){
	DisableIrq();
    ISP_delay_ms(1);
	memset( &MEMORY_TAB , 0 , sizeof( MEMORY_TAB));
	
	if( MemoryInfo.Voice_Load ){
		MEMORY_TAB[0] = set_bit1( GetRomOther() , 2 );
	}else{
		MEMORY_TAB[0] = set_bit0( GetRomOther() , 2 );
	}
    
    ISPWrite(0xFF6E , MEMORY_TAB[0]);
        
    ISP_delay_ms(1);
    EnableIrq();
}
/**********************************************************
* @brief 试纸过期设置
* @param 
* @return 
**********************************************************/
void EP_Set_Write(void){
	DisableIrq();
    ISP_delay_ms(1);
	memset( &MEMORY_TAB , 0 , sizeof( MEMORY_TAB));
	
	if( MemoryInfo.EP_Load ){
		MEMORY_TAB[0] = set_bit1( GetRomOther() , 3 );
	}else{
		MEMORY_TAB[0] = set_bit0( GetRomOther() , 3 );
	}
    
    ISPWrite(0xFF6E , MEMORY_TAB[0]);
        
    ISP_delay_ms(1);
    EnableIrq();
}

/**********************************************************
* @brief 写入血糖记忆、index++
* @param 
* @return 
**********************************************************/
void Memory_Write(void)
{
	uint16_t isp_temp;
	uint32_t temp_stamp;
	
	timToStamp(&temp_stamp , MemoryInfo);
	memset( &MEMORY_TAB , 0 , sizeof( MEMORY_TAB));
	MEMORY_TAB[0] = 0x99;  //血糖[低位]
    MEMORY_TAB[1] = 0x99; //血糖[高位]
    MEMORY_TAB[2] = temp_stamp; //时间戳    
    MEMORY_TAB[3] = temp_stamp>>8;  
    MEMORY_TAB[4] = temp_stamp>>16;  
    MEMORY_TAB[5] = temp_stamp>>24;  
	MEMORY_TAB[6] = 1;  //苹果类型
	MEMORY_TAB[7] = 0;  //暂留
    
    DisableIrq();
    ISP_delay_ms(10);                   
    
    MEM_Addr = ROM_MEMORY_ADDR;  
	MEM_Addr += (MemoryInfo.Member_Index_Load*8);
    
    if(MEM_Addr < 0xFF40)                   //不能超出范围    
    {
		for(isp_temp=0;isp_temp<8;isp_temp++)      
        {
			ISPWrite(MEM_Addr , MEMORY_TAB[isp_temp]);
            MEM_Addr++;
        }

        if(MemoryInfo.Member_Index_Load >= (MEMORY_NUMBER-1))           //记忆数量-1
        {
            MemoryInfo.Member_Index_Load = 0;
        }
        else	
        {
            //MemoryInfo.Member_Index_Load++;	
        }
    }
	  
    MEM_Addr=0xFF6E;
    MEMORY_TAB[0] = MemoryInfo.Member_Index_Load;
	MEMORY_TAB[1] = (MemoryInfo.Member_Index_Load)>>8;
    ISPWrite(MEM_Addr , MEMORY_TAB[0]);
        
	MEM_Addr=0xFF6F;
	ISPWrite(MEM_Addr , MEMORY_TAB[1]);
		
    ISP_delay_ms(10);
    EnableIrq();
}
