#ifndef __APP_FLASH_H__
#define __APP_FLASH_H__

#include "type_define.h"

typedef struct
{
	uint16_t nAREF_mV; //0x1000
	uint16_t dacCalWork;
	uint16_t dacCalThird;
	uint16_t adcRefValue;
//	float    calAdc;
//	uint8_t SetFunction_Unit;//0x1004  
//	uint8_t SetFunction_TemperatureUnit;//0x1005  
//	uint8_t SetFunction_DateTimeFormat;//0x1006    
//	uint8_t SetFunction_SetYearStart;//0x1006        
//	uint8_t  SetFunction_SetYearEnd;//0x1006        
//	uint16_t SetFunction_MemorySize;//0x
//	uint8_t reserved;
//	uint8_t SetFunction_BLEPairingMode;	
//	uint8_t BLE_Enable;
}flashCalib_t;

typedef struct __MeterInformation
{
	uint8_t Unit;
	uint8_t Code;
	uint8_t Flag_DisplayDebugMessage; // 01 for set, and ohter for not set
	uint8_t bHourMode;// 0x00:24H Mode
	uint8_t reserved_1;// 0x00:24H Mode
	uint8_t reserved_2;// 0x00:24H Mode
	uint8_t reserved_3;// 0x00:24H Mode
	uint8_t reserved_5;// 0x00:24H Mode
	uint8_t code2_qc_flag;
	uint8_t code8_qc_flag;    
	uint16_t SQNumber;
}FlashStorage_BGM_InfoTypeDef;

typedef struct 
{
	uint16_t Glucose: 10;
	uint16_t FlagPreMeeal: 1;	
	uint16_t FlagPostMeeal: 1;	
	uint16_t FlagFasting: 1;
	uint16_t FlagCS: 1;				
	uint16_t FlagKetone: 1;
	uint16_t FlagTargetRange: 1;	
}FlashStorage_GlucoseInfo_TypeDef;

typedef struct 
{
	//RTC_TimeInfoTypeDef TimeInfo;
	FlashStorage_GlucoseInfo_TypeDef GlucoseFlag;	
	uint16_t nTimeOffset;	
}FlashStorage_GlucoseResult_TypeDef;

extern FlashStorage_GlucoseResult_TypeDef g_CurrentR;	
extern FlashStorage_BGM_InfoTypeDef g_CurrentS;
extern flashCalib_t g_calData;	

//app_flash.c
void InitCalibData(void);
void SetNref(uint16_t value);
uint16_t GetNref(void);
void CalibDaoValue(void);




#endif