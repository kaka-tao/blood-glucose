#include "main.h"

#define  START_WAIT_CNTS      2000U



SysInfo_t  g_SysInfo;
//static Sleep_t  g_SleepInfo = {
//       .wakeupsource = E_WakeUpSource_No,
//       .state = E_GoingToSleep,
//       .cnts100msToSleep = 1,	
//};
static Sleep_t  g_SleepInfo = {E_WakeUpSource_No,E_GoingToSleep,1};

/**********************************************************
* @brief 初始化系统变量
* @param 
* @return 
**********************************************************/
void SleepInitSysVar(void)
{
	memset(&g_SysInfo,0,sizeof(g_SysInfo));
	g_SleepInfo.wakeupsource = E_WakeUpSource_No;
	g_SleepInfo.state = E_GoingToSleep;
	g_SleepInfo.cnts100msToSleep = 1;
}
/**********************************************************
* @brief 清零系统tick
* @param 
* @return 
**********************************************************/
void ClearGlobalCount(void)
{
  g_SysInfo.globalCount = 0;
}
/**********************************************************
* @brief ++tick
* @param 
* @return 
**********************************************************/
void IncGlobalCount(void)
{
  g_SysInfo.globalCount++;
}
/**********************************************************
* @brief 查询系统tick
* @param 
* @return 
**********************************************************/
uint32_t GetGlobalCount(void)
{
  return g_SysInfo.globalCount;
}
///**********************************************************
//* @brief 获取状态
//* @param 
//* @return 
//**********************************************************/
uint8_t GetGameStatus(void)
{
  return g_SysInfo.mainState;
}

///**********************************************************
//* @brief 改变状态
//* @param 
//* @return 
//**********************************************************/
void ChangeGameStatus(Game_Status nextStatus)
{
   g_SysInfo.mainState = nextStatus;
}
/**********************************************************
* @brief 改变主状态机的状态
* @param 
* @return 
**********************************************************/
void ChangeMainState(e_Main_State_t nextState)
{
   g_SysInfo.state = nextState;
}
/**********************************************************
* @brief 电池电量处理
* @param 
* @return 
**********************************************************/
void TaskProBat(void)
{
  BatGetLevel();//读取电池电量

}

/**********************************************************
* @brief 处理开机后的等待
* @param 
* @return 
**********************************************************/
static void Pro_StartWait(void)
{
	static e_WakeUpSource_t    startState = E_WakeUpSource_No;
	static e_WakeUpSource_t    wakeSource = E_WakeUpSource_No;
	
	if(startState == 0)
	{
		if(BUTTON_DOWN == GetDet_State()){    //有试纸条插入
			startState = E_WakeUpSource_Det;
		}
		else if(BUTTON_DOWN == GetKey_S_State()){
			startState = E_WakeUpSource_KeyS;
		}
		else if(BUTTON_DOWN == GetKey_Left_State()){
			startState = E_WakeUpSource_KeyL;
		}
		else if(BUTTON_DOWN == GetKey_Right_State()){
			startState = E_WakeUpSource_KeyR;
		}
	}
	if((g_SysInfo.globalCount >= 100) && (startState != E_WakeUpSource_No))
	{
		if(BUTTON_DOWN == GetDet_State() && startState == E_WakeUpSource_Det){    //有试纸条插入
			wakeSource = E_WakeUpSource_Det;
		}
		else if(BUTTON_DOWN == GetKey_S_State() && startState == E_WakeUpSource_KeyS){
			wakeSource = E_WakeUpSource_KeyS;
		}
		else if(BUTTON_DOWN == GetKey_Left_State() && startState == E_WakeUpSource_KeyL){
			wakeSource = E_WakeUpSource_KeyL;
		}
		else if(BUTTON_DOWN == GetKey_Right_State() && startState == E_WakeUpSource_KeyR){
			wakeSource = E_WakeUpSource_KeyR;
		}
	}

	if(g_SysInfo.globalCount > START_WAIT_CNTS)//等待时间到达
	{
	   startState = E_WakeUpSource_No;
		/////////////
//		 wakeSource = E_WakeUpSource_Det; //debug
		/////////////////
		 InitCalibData();
		
		 if(wakeSource == E_WakeUpSource_Det){
			  wakeSource = E_WakeUpSource_No;
		    ChangeMainState(E_DetInput);
		 }
		 else if(wakeSource == E_WakeUpSource_KeyS){
		    wakeSource = E_WakeUpSource_No;
		 
		 }
		 else if(wakeSource == E_WakeUpSource_KeyL){
		    wakeSource = E_WakeUpSource_No;
		 
		 }
		 else if(wakeSource == E_WakeUpSource_KeyR){
		    wakeSource = E_WakeUpSource_No;
		 
		 }
	   
	}

}
/**********************************************************
* @brief 血糖测量主状态机
* @param 
* @return 
**********************************************************/
#if 0
void MainStatePro(void)
{
  switch(g_SysInfo.state)
	{
		case E_ProError:
		{
		   ProSysErr(&g_SysInfo);
		}
		break;
		case E_Cablic_Dac:
		{
		   CalibDaoValue();
		}
		break;
		case E_StartWait:
		{
		   Pro_StartWait();
		}
		break;
		case E_DetInput:
		{
		   Pro_DetInput();
		}
		break;
		case E_ValidationStrip://检测试纸条
		{
		  MeasurementValidateStrip(&g_SysInfo);
	    
		}
		break;
		case E_Ready_Measure://检测是否有血液滴入
		case E_Measure_Start:
		{
		     NisoMeasurement(&g_SysInfo);
		}
	  break;
		
		case E_Measure_Confirm:
		{
			   MeasurementConfirmResult(&g_SysInfo);
		}
	  break;
		default:
		break;
	}

}
#endif
/**********************************************************
* @brief 处理Sleep
* @param 
* @return 
**********************************************************/
static void ProSleep(SysInfo_t *pInfo)
{
	static uint32_t lastSysCnts = 0;
	uint32_t newSysCnts = 0;
	
	newSysCnts = GetGlobalCount();
	
  if(E_GoingToSleep == g_SleepInfo.state
		 &&(newSysCnts - lastSysCnts) >= 100)
  {
		 lastSysCnts = newSysCnts;
	   if(--g_SleepInfo.cnts100msToSleep == 0)
		 {
		    g_SleepInfo.state = E_Sleeping;		  
			 
        HwBeforeSleepInit();
        SysEnterStop();   //关机时停在此处
        HwAfterSleepInit();	
			 
			  pInfo->mainState = E_Mstate_ReadMem;
			  memset(&pInfo->memInfo,0,sizeof(pInfo->memInfo));
			 
//			  g_SysInfo.state = E_Cablic_Dac;
//	      g_SleepInfo.state = E_NotSleep;	
//			 
//        g_calData.dacCalWork = 409;
//        AdcConnect_CalibDao();
//        SetDacValue(g_calData.dacCalWork);			 
		 }
	}		
}
/**********************************************************
* @brief 处理ReadMem
* @param 
* @return 
**********************************************************/
static void ProReadMem(SysInfo_t *pInfo)
{
	static uint32_t lastSysCnts = 0;
	uint32_t newSysCnts = 0;
	
	newSysCnts = GetGlobalCount();
	
	if((newSysCnts - lastSysCnts) >= 100)
  {
		 if(pInfo->memInfo.stateCnt100ms == 0)
		 {
		   //通知全显
		 }
		 else if(pInfo->memInfo.stateCnt100ms == ALL_DIS_100MS_CNTS)
		 {
		   //通知结束全显，显示年/月/时/分
		 }
		 else if(pInfo->memInfo.stateCnt100ms == (ALL_DIS_100MS_CNTS + DATA_DIS_100MS_CNTS))
		 {
		   //通知显示一日平均的记忆值
		 }
		 pInfo->memInfo.stateCnt100ms++;
	}		
}
/**********************************************************
* @brief 处理参数设置
* @param 
* @return 
**********************************************************/
static void ProSetPara(SysInfo_t *pInfo)
{
	static uint32_t lastSysCnts = 0;
	uint32_t newSysCnts = 0;
	
	newSysCnts = GetGlobalCount();
	
	if((newSysCnts - lastSysCnts) >= 100)
  {

	}		
}
/**********************************************************
* @brief 主流程状态
* @param 
* @return 
**********************************************************/
void MainStatePro(const SysInfo_t *pInfo)
{
	switch(pInfo->mainState)
	{
		case E_Mstate_Sleep:
       ProSleep(&g_SysInfo);			
		break;
		case E_Mstate_ReadMem:
       ProReadMem(&g_SysInfo);
		break;
		case E_Mstate_SetPara:
       ProSetPara(&g_SysInfo);
		break;
		case E_Mstate_SetEp:
		break;
		case E_Mstate_Measure:
		break;
		case E_Mstate_SetAlarmAfterDinner:
		break;
		case E_Mstate_SetAlarmUser:

		break;
		case E_Mstate_Err:

		break;
		default:
			break;
	}
}

