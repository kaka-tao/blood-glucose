#include "main.h"
#include "MultiButton.h"
#include "app_lcd.h"

static button xdata button_S;
static button xdata button_LEFT;
static button xdata button_RIGHT;
static button xdata button_Strip;

/**********************************************************
* @brief 注册按键 
* @param 
* @return 
**********************************************************/
void RegButton(void)
{
	ButtonInit(&button_S, GetKey_S_State, BUTTON_DOWN);
	ButtonStart(&button_S);
	
	ButtonInit(&button_LEFT, GetKey_Left_State, BUTTON_DOWN);
	ButtonStart(&button_LEFT);
	
	ButtonInit(&button_RIGHT, GetKey_Right_State, BUTTON_DOWN);
	ButtonStart(&button_RIGHT);
	
	ButtonInit(&button_Strip, GetDet_State, BUTTON_DOWN);
	ButtonStart(&button_Strip);
}
/**********************************************************
* @brief 处理按键 S 按下
* @param 
* @return 
**********************************************************/
static void ProEventButton_S_down(SysInfo_t *pInfo,DisInfo_t *pLcdInfo)
{	
 if(pInfo->mainState == E_Mstate_ReadMem)
 {
   if(pInfo->memInfo.stateCnt100ms > (ALL_DIS_100MS_CNTS + DATA_DIS_100MS_CNTS))
	 {
	 	 pInfo->mainState = E_Mstate_Sleep;
		 //通知清屏
		 SleepInitSysVar();//进入sleep前的处理
	 }
 }
 
}
/**********************************************************
* @brief 处理按键 S 弹起
* @param 
* @return 
**********************************************************/
static void ProEventButton_S_up(SysInfo_t *pInfo,DisInfo_t *pLcdInfo)
{
	if(pInfo->mainState == E_Mstate_ReadMem){
		InitDisplayInfo();		
		pInfo->mainState = E_Mstate_Sleep;
		SleepInitSysVar();//进入sleep前的处理
	}
	else if(pInfo->mainState == E_Mstate_SetPara)
	{
		Set_Time_Reload();
		switch( Get_Main_Stage() ){
			case 0:
				if( Get_Set_Start_Flag() ){
					Set_Main_Stage(1);
					ClearLcdRam();
				}else{
					InitDisplayInfo();
					SleepInitSysVar();
				}
				break;
				
			case 1:
				Set_Main_Stage(2);
				ClearLcdRam();
				break;
			
			case 2:
				Set_Main_Stage(3);
				Year_Write();
				ClearLcdRam();
				break;
			
			case 3:
				Set_Main_Stage(4);
				Month_Write();
				Day_Write();
				ClearLcdRam();
				break;
			
			case 4:
				Set_Main_Stage(5);
				Day_Write();
				ClearLcdRam();
				break;
			
			case 5:
				Set_Main_Stage(6);
				Hour_Set_Write();
				ClearLcdRam();
				break;
			
			case 6:
				Set_Main_Stage(7);
				Hour_Write();
				ClearLcdRam();
				break;
			
			case 7:
				Set_Main_Stage(8);
				Minute_Write();
				ClearLcdRam();
				break;
			
			case 8:
				Set_Main_Stage(9);
				Voice_Set_Write();
				ClearLcdRam();
				break;
			
			case 9:
				Set_Main_Stage(10);
				EP_Set_Write();
				ClearLcdRam();
				break;
			
			case 10:
				if( Get_Memory_Low_Set() ){
					Set_Main_Stage(11);
				}else{
					Set_Main_Stage(12);
				}
				Low_Set_Write();
				ClearLcdRam();
			break;
						
			case 11:
				Set_Main_Stage(12);
				Low_Value_Write();
				ClearLcdRam();
			break;
					
			case 12:
				if( Get_Memory_Low_Set() ){
					Set_Main_Stage(13);
				}else{
					Set_Main_Stage(1);
				}
				High_Set_Write();
				ClearLcdRam();
			break;
						
			case 13:
				Set_Main_Stage(1);
				High_Value_Write();
				ClearLcdRam();
			break;			
						
			default:
				break;
		}
	}
}
/**********************************************************
* @brief 处理按键 S 长按
* @param 
* @return 
**********************************************************/
static void ProEventButton_S_longPress(SysInfo_t *pInfo)
{
#if 0
	if( (GetGameStatus() == 0 ) || ( GetGameStatus() == 1)){
		InitDisplayInfo();
		ClearLcdRam();
		ChangeGameStatus(E_SetGame);
		Set_Main_Count(1);
		Set_Blink_Flag(1);
	}else if( GetGameStatus() == 2 ){
		InitDisplayInfo();
		SleepInitSysVar();
	}
#endif
  if(pInfo->mainState == E_Mstate_ReadMem)
  {
	  pInfo->mainState = E_Mstate_SetPara;
	
	}		
}

/**********************************************************
* @brief 处理按键 left 按下
* @param 
* @return 
**********************************************************/
static void ProEventButton_left_down(SysInfo_t *pInfo)
{
#if 0
	if(pInfo->mainState == E_Mstate_Sleep)
	{
    //全显
	}
#endif
	if( (Get_Main_Stage() == 0) && (GetGameStatus() == 0)){
		Set_Update_Flag(1);
		Set_Main_Stage(1);
		Set_Main_Count(4);
	}
}

/**********************************************************
* @brief 处理按键 left 弹起
* @param 
* @return 
**********************************************************/
static void ProEventButton_left_up(SysInfo_t *pInfo)
{
		if(GetGameStatus()== 1){
		if( Get_Main_Stage() > 0 ){
			Sub_Main_Stage();
		}
	}else if( GetGameStatus() == 2){
		Set_Time_Reload();
		switch( Get_Main_Stage() ){
			case 0:
				Anti_Set_Start_Flag();
				break;
			
			case 1:
				break;
				
			case 2:
				Sub_Memory_Year();
				break;
			
			case 3:
				Sub_Memory_Month();
				break;
			
			case 4:
				Sub_Memory_Day();
				break;
			
			case 5:
				Anti_Memory_Time_Set();
				break;
			
			case 6:
				Sub_Memory_Hour();
				break;
			
			case 7:
				Sub_Memory_Minute();
				break;
			
			case 8:
				Anti_Memory_Voice_Set();
				break;
			
			case 9:
				Anti_Memory_EP_Set();
				break;
				
			case 10:
				Anti_Memory_Low_Set();
			break;
					
			case 11:
				Sub_Memory_Low();
			break;
					
			case 12:
				Anti_Memory_High_Set();
			break;
					
			case 13:
				Sub_Memory_High();
			break;
		}
	}
}
/**********************************************************
* @brief 处理按键 left 长按
* @param 
* @return 
**********************************************************/
static void ProEventButton_left_longPress(SysInfo_t *pInfo)
{

}
/**********************************************************
* @brief 处理按键 right 按下
* @param 
* @return 
**********************************************************/
static void ProEventButton_right_down(SysInfo_t *pInfo)
{
#if 0
	if(pInfo->mainState == E_Mstate_Sleep)
	{
    //全显
	}
#endif
	if( (Get_Main_Stage()== 0) && (GetGameStatus() == 0)){
		Set_Update_Flag(1);
		Set_Main_Stage(1);
		Set_Main_Count(4);
	}
}
/**********************************************************
* @brief 处理按键 right 弹起
* @param 
* @return 
**********************************************************/
static void ProEventButton_right_up(SysInfo_t *pInfo)
{
			if(GetGameStatus()== 1){
				if( Get_Main_Stage() < (19 + Get_Memory_Index()) ){
					Add_Main_Stage();
				}
			}else if(GetGameStatus() == 2){
				Set_Time_Reload();
				switch( Get_Main_Stage() ){
					case 0://START 0N-OFF
						Anti_Set_Start_Flag();
						break;
					
					case 1://BLUE ON-OFF
						break;
					
					case 2:
						Add_Memory_Year();
						break;
					
					case 3:
						Add_Memory_Month();
						break;
					
					case 4:
						Add_Memory_Day();
						break;
					
					case 5:
						Anti_Memory_Time_Set();
						break;
					
					case 6:
						Add_Memory_Hour();
						break;
					
					case 7:
						Add_Memory_Minute();
						break;
					
					case 8:
						Anti_Memory_Voice_Set();
						break;
					
					case 9:
						Anti_Memory_EP_Set();
						break;				
				}	
			}
}
/**********************************************************
* @brief 处理按键 right 长按
* @param 
* @return 
**********************************************************/
static void ProEventButton_right_longPress(SysInfo_t *pInfo)
{

}
/**********************************************************
* @brief 按键扫描任务 
* @param 
* @return 
**********************************************************/
void TaskButton(void)
{
	PressEvent btn_S_event,btn_Left_event,btn_Right_event,btn_strip_event;
	
	ButtonScan();
	
	btn_S_event = GetButtonEvent(&button_S);
	btn_Left_event = GetButtonEvent(&button_LEFT);
	btn_Right_event = GetButtonEvent(&button_RIGHT);
	btn_strip_event = GetButtonEvent(&button_Strip); 
	
	switch(btn_strip_event)
	{
	  case PRESS_DOWN://试纸插入
		{
		
		}
		break;
	  case PRESS_UP:
		{
		
		}
		break;
	}
	switch(btn_S_event)
	{
		case PRESS_DOWN:
		{
			UartTxString("button S down\n"); 
      ProEventButton_S_down(&g_SysInfo,&displayInfo);
		}
		break; 
		case PRESS_UP: 
		{
			UartTxString("button S UP\n"); 
      ProEventButton_S_up(&g_SysInfo);
			ProEventButton_S_down(&g_SysInfo,&displayInfo);
		}
		break; 
		case LONG_RRESS_START: 
		{
			UartTxString("button S long press\n"); 
      ProEventButton_S_longPress(&g_SysInfo);
		}
		break; 
		case LONG_PRESS_HOLD: 
			UartTxString("button S long hold\n"); 
		break;
    default:
 		break;	
  }
	
	switch(btn_Left_event)
	{
		case PRESS_DOWN:
		{
			UartTxString("button left down\n"); 
      ProEventButton_left_down(&g_SysInfo); 
		}
		break; 
		case PRESS_UP: 
		  ProEventButton_left_up(&g_SysInfo);
		break; 
		case LONG_RRESS_START: 
		  ProEventButton_left_longPress(&g_SysInfo); 
		break; 
		case LONG_PRESS_HOLD: 
		break; 
    default:
 		break;
  }
	switch(btn_Right_event)
	{
		case PRESS_DOWN:
			ProEventButton_right_down(&g_SysInfo);
		break; 
		case PRESS_UP: 
      ProEventButton_right_up(&g_SysInfo);
		break; 
		case LONG_RRESS_START: 
			ProEventButton_right_longPress(&g_SysInfo);
		break; 
		case LONG_PRESS_HOLD: 
			//rt_kprintf("button long press hold\n");
		break;
    default:
 		break;		
  }
}