#ifndef __APP_MEASURE_H__
#define __APP_MEASURE_H_

#include "type_define.h"

typedef void (*pFunction)(void);

typedef enum
{
	Measurement_Glucose = 0,
	Measurement_Ketone
}Measurement_TypeDef;

typedef struct
{
  uint8_t    startFlg;
	uint8_t    completeFlg;
  uint16_t   nisoAdcValue[8];
	
	uint16_t    DacCount;
	uint8_t    AdcCount;
	
	int16_t    workResult;
  int16_t    thirdResult;
}NisoData_t;

typedef struct 
{
	uint8_t measurementCount;
	uint8_t  VoltageStepIndex;
	uint8_t thirdConfirm;		//for NISO study JIG mode
	uint8_t thirdCheckDisable;
	Measurement_TypeDef Type;
	uint16_t thirdDetectionTime;
	uint16_t RawDataCount;
	uint16_t StripJigDataCount;
	uint16_t workConfirmCount;
	uint16_t thirdConfirmCount;
	int16_t thirdAdcFirst;
	int16_t thirdAdcSecond;
	uint16_t dacThird;
	uint16_t PotentialAC;
	pFunction GetFeature;
	pFunction CurrnetEquation;
	pFunction AlgorithmEquation;
	pFunction CheckError8;

}NisoSequence_t;


#define CNT_0010_SEC			(uint16_t)4	
#define CNT_05_SEC				(uint16_t)200	
#define CNT_075_SEC				(uint16_t)300	
#define CNT_10_SEC			 	(uint16_t)400		// 1sec, 400Hz  
#define CNT_17_SEC				(uint16_t)680			 //kimte.2013.09.02 : 400Hz + 	temperature smapling 
#define CNT_20_SEC				(uint16_t)800	
#define CNT_25_SEC				(uint16_t)1000			 //kimte.2013.09.02 : 400Hz + 	temperature smapling 
#define CNT_251_SEC				(uint16_t)1004			 //kimte.2013.09.02 : 400Hz + 	temperature smapling 
#define CNT_30_SEC				(uint16_t)1200
#define CNT_31_SEC				(uint16_t)1240	
#define CNT_32_SEC			 	(uint16_t)1280	
#define CNT_35_SEC				(uint16_t)1400
#define CNT_351_SEC				(uint16_t)1404
#define CNT_40_SEC				(uint16_t)1600
#define CNT_43_SEC				(uint16_t)1720
#define CNT_46_SEC				(uint16_t)1840
#define CNT_47_SEC				(uint16_t)1880
#define CNT_48_SEC				(uint16_t)1920
#define CNT_49_SEC				(uint16_t)1960
#define CNT_50_SEC				(uint16_t)2000
#define CNT_55_SEC				(uint16_t)2200
#define CNT_56_SEC	 			(uint16_t)2240
#define CNT_57_SEC	 			(uint16_t)2280
#define CNT_58_SEC	 			(uint16_t)2320
#define CNT_60_SEC	 			(uint16_t)2400
#define CNT_62_SEC	 			(uint16_t)2480
#define CNT_62_25_SEC			(uint16_t)2490
#define CNT_62_75_SEC			(uint16_t)2510
#define CNT_63_SEC				(uint16_t)2520 
#define CNT_64_SEC 				(uint16_t)2560 //2900 //2560 
#define CNT_70_SEC				(uint16_t)2800
#define CNT_80_SEC				(uint16_t)3200
#define CNT_100_SEC				(uint16_t)4000
#define CNT_105_SEC				(uint16_t)4200


/** define 
---------------------------------------------------------------------*/
#define Size_Of_Raw_Data	(uint16_t)1200
//#define Size_Of_Raw_Data	(uint16_t)1600


/** typedefs
---------------------------------------------------------------------*/
typedef struct 
{
	uint8_t FullMemory;
	uint16_t MaxData;					  

	uint16_t nCount;                    // nCount?? 0???? maxdata ??? ???? ?? ???.
	uint16_t SavePoint;					//Save Point?? 0 ???? max data +ReservedDataCnt ??? ???? ?? ???.        
	uint16_t OldestDataPoint;			//OldestData Point ?? full memory ???? 0 ???  ??? ???? Save Point-1000?? ???? ??????.
							// - ???? ?????? ???  FlashDataEndPoint ???? ??? ???? ?????? ???.
	uint16_t ReservedDataCnt;			     //smw.2015.01.05 : ReservedDataCnt = FlashDataEndPoint - maxdata.
	uint16_t FlashDataEndPoint;					  
	uint16_t LatestSQNumber;					       

	uint16_t Average1daysTotal;		    // Average of data after meal for 1 day
	uint16_t Count1daysTotal;			// the number of glucose data after meal for 1 day   
	uint16_t Average7daysTotal;
	uint16_t Count7daysTotal;       
	uint16_t Average14daysTotal;
	uint16_t Count14daysTotal;   
	uint16_t Average30daysTotal;
	uint16_t Count30daysTotal;   
	uint16_t Average90daysTotal;
	uint16_t Count90daysTotal;   

	uint16_t Average1daysPreMeal;			    // Average of data before meal for 1 day
	uint16_t Count1daysPreMeal;				// the number of glucose data before meal for 1 day
	uint16_t Average7daysPreMeal;			    // Average of data before meal for 1 day
	uint16_t Count7daysPreMeal;				// the number of glucose data before meal for 1 day
	uint16_t Average14daysPreMeal;			    // Average of data before meal for 1 day
	uint16_t Count14daysPreMeal;				// the number of glucose data before meal for 1 day
	uint16_t Average30daysPreMeal;			    // Average of data before meal for 1 day
	uint16_t Count30daysPreMeal;				// the number of glucose data before meal for 1 day
	uint16_t Average90daysPreMeal;			    // Average of data before meal for 1 day
	uint16_t Count90daysPreMeal;				// the number of glucose data before meal for 1 day

	uint16_t Average1daysPostMeal;	    // Average of data after meal for 1 day
	uint16_t Count1daysPostMeal;		// the number of glucose data after meal for 1 day
	uint16_t Average7daysPostMeal;	    // Average of data after meal for 1 day
	uint16_t Count7daysPostMeal;		// the number of glucose data after meal for 1 day
	uint16_t Average14daysPostMeal;	    // Average of data after meal for 1 day
	uint16_t Count14daysPostMeal;		// the number of glucose data after meal for 1 day
	uint16_t Average30daysPostMeal;	    // Average of data after meal for 1 day
	uint16_t Count30daysPostMeal;		// the number of glucose data after meal for 1 day
	uint16_t Average90daysPostMeal;	    // Average of data after meal for 1 day
	uint16_t Count90daysPostMeal;		// the number of glucose data after meal for 1 day

	uint16_t Average1daysFasting;		    // Average of data after meal for 1 day
	uint16_t Count1daysFasting;			// the number of glucose data after meal for 1 day    
	uint16_t Average7daysFasting;		    // Average of data after meal for 1 day
	uint16_t Count7daysFasting;			// the number of glucose data after meal for 1 day    
	uint16_t Average14daysFasting;		    // Average of data after meal for 1 day
	uint16_t Count14daysFasting;			// the number of glucose data after meal for 1 day    
	uint16_t Average30daysFasting;		    // Average of data after meal for 1 day
	uint16_t Count30daysFasting;			// the number of glucose data after meal for 1 day    
	uint16_t Average90daysFasting;		    // Average of data after meal for 1 day
	uint16_t Count90daysFasting;			// the number of glucose data after meal for 1 day        
	
//	uint8_t Q_Copy[50];	 
//	uint8_t Q_Counter;				//BLE Q Copy Counter	
//	uint8_t BLEQArray[50];
	//QueueVariables BLEQ;
	//char  BLE_SendDataBuffer[50];
} Average_ListTypeDef;

typedef union
{
	Average_ListTypeDef Average_List;
	uint16_t GlucoseRawData[Size_Of_Raw_Data];// int16_t 
}RAM_DataTypeDef;



#endif

