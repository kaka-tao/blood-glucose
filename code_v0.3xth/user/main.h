#ifndef __MAIN_H__
#define __MAIN_H__

#include <string.h>
#include "type_define.h"
#include "bsp_GPIO.h"
#include "bsp_key.h"
#include "bsp_lcd.h"
#include "bsp_analog.h"
#include "bsp_timer.h"
#include "bsp_usart.h"
#include "bsp_isp.h"

#include "app_flash.h"
#include "app_measure.h"
#include "app_lcd.h"
#include "app_isp.h"

#define DEBUG_TIMER          0  //debug定时器

#define CONTROL_SOLUTION     0   //质控液模式 
#define GDH

#define BATRATIO	                ((uint8_t)3)
#define TEMP_LOW			            ((int16_t)55)		
#define TEMP_HIGH			            ((int16_t)445)	

#define M_FEEDBACK_WORK_R         ((float)20.0f)
#define M_FEEDBACK_THIRD_R        ((float)11.0f)
	
#define M_OffSET_200mV				    ((float)200.0f)
#define M_TEMP_LOW			          ((float)55.0f)		
#define M_TEMP_HIGH			          ((float)445.0f)	

#define M_THRESHOLD_KETONE_WORK		((float)56.8f)	
#define M_THRESHOLD_WORK 			    ((float)19.0f) // 190nA
#define M_THRESHOLD_THIRD			    ((float)6.0f)//60n
#define M_THRESHOLD_THIRD_ADC	  	((int16_t)140)  // ADC value

#if defined(GOx)
#define M_THRESHOLD_ERR_1			    ((float)650.0f) 
#elif defined (GDH)
#define M_THRESHOLD_ERR_1			    ((float)1000.0f) 
#endif

typedef enum{
	E_afterSleep = 0,
	E_Cablic_Dac,
	E_StartWait,
	E_DetInput,
	E_ValidationStrip,
	E_Ready_Measure,
	E_Measure_Start,
	E_Measure_Confirm,
	E_ProError,
}e_Main_State_t;

typedef enum
{
	e_BatLevel_High = ((uint16_t)300),		       // 3V
	e_BatLevel_Low = ((uint16_t)250),  		       // 2.5V
	e_BatLevel_Shutdown = ((uint16_t)240) 		   // 2.4V
}e_BatLevel_t;


typedef enum{
	E_EndGame = 0,//结束状态
	E_LoadGame,//读取记忆状态
	E_SetGame,//设置
	E_StartGame,//测量状态
}Game_Status;

/************************************************************
子状态->SleepInfo
*************************************************************/
typedef enum{
  E_Sleeping = 0,
	E_NotSleep,
	E_GoingToSleep,
}e_SleepState_t;

typedef enum{
	E_WakeUpSource_No  = 0,
  E_WakeUpSource_Det,
	E_WakeUpSource_KeyS,
	E_WakeUpSource_KeyL,
	E_WakeUpSource_KeyR,
}e_WakeUpSource_t;

typedef struct{
	 e_WakeUpSource_t wakeupsource;
   e_SleepState_t   state;
   uint8_t          cnts100msToSleep;
}Sleep_t;

/************************************************************
子状态->读记忆   MemInfo
*************************************************************/
#define  ALL_DIS_100MS_CNTS    20u
#define  DATA_DIS_100MS_CNTS   2u
typedef struct{
   uint16_t  stateCnt100ms;
}MemInfo_t;



/************************************************************
子状态->设置参数
*************************************************************/
typedef enum{
	E_SetPara_Flag = 0,              //确认是否进入设置状态模式：yes/no
	E_SetPara_Ble,                   //设置蓝牙相关
	
	E_SetPara_Year,                  //设置年份
	E_SetPara_Month,                 //设置月
	E_SetPara_Day,                   //设置日
	E_SetPara_12_24,                 //设置12小时或24小时模式
	E_SetPara_Hour,                  //设置时
	E_SetPara_Min,                   //设置分
	
	E_SetPara_Beep,                  //设置蜂鸣器开关
	E_SetPara_Ep,                    //设置有效日期开关   EP
	
	E_SetPara_ResultLoSwith,         //设置血糖值下限开关
	E_SetPara_ResultLoVal,           //设置血糖值下限值
	E_SetPara_ResultHiSwith,         //设置血糖值上限
	E_SetPara_ResultHiVal,           //设置血糖值上限
}e_SetPara_State_t;

typedef struct{
   e_SetPara_State_t  setParaState;
}SetParaInfo_t;

/********************
子状态->测量
*********************/
typedef enum{
	MeasureState_DetInput = 0,       //试纸条插入
	MeasureState_ValidationStrip,    //读取试纸条信息
	MeasureState_Ready,              //测量前的准备
	MeasureState_Start,              //测量中(检波)
	MeasureState_Confirm,            //计算测量结果
}Measure_State_t;
/********************
子状态->设置有效期
*********************/



/********************
子状态->设置餐后闹钟
*********************/



/********************
子状态->设置定时闹钟
*********************/
typedef enum{
	AlarmState_User1 = 0,            //定时闹钟1
	AlarmState_User2,                //定时闹钟2
	AlarmState_User3,                //定时闹钟3
}AlarmUser_State_t;

/********************
子状态->处理错误
*********************/
typedef enum{
	Error_no = 0,                    
	Error_1_Used_Strip,              //已使用过的试纸
	Error_2_Not_Ready,               //没准备好滴入了血液或质控液
	Error_3_Invalid_Temperature,     //温度超范围
	Error_4_Not_Sufficient_Blood,    //被测液黏稠或血量不足
	Error_5_Invalid_Strip_Code,      //非指定血糖试纸
	Error_6_Sensor_Out_Of_Work,      //血糖仪故障
	Error_7_Communication_Data,      //蓝牙连接故障
	Error_8_Invalid_Raw_Data,        //检测过程发生电路错误
}e_Err_Kind_t;




/************************************************************
系统主状态
*************************************************************/
typedef enum{
	E_Mstate_Sleep = 0,              //睡眠
	E_Mstate_ReadMem,                //调记忆
	E_Mstate_SetPara,                //设置参数
	E_Mstate_Measure,                //测量
	E_Mstate_SetEp,                  //设置有效期
	
	E_Mstate_SetAlarmAfterDinner,    //设置餐后闹钟
	E_Mstate_SetAlarmUser,           //设置定时闹钟
	
	E_Mstate_Err,                    //处理错误
}Main_State_t;

typedef struct{
	 Main_State_t     mainState;     //主状态
	 MemInfo_t        memInfo;       //子状态->mem
	 SetParaInfo_t    setParaInfo;   //子状态->setPara
	
	
   volatile e_Main_State_t   state;
   volatile uint32_t         globalCount;

	 int16_t          realTemp;      //温度
	 e_Err_Kind_t     errKind;
}SysInfo_t;




typedef struct{
   e_BatLevel_t batLevel;

}Analog_t;

typedef enum{
  CH_EXT_TEMP_SENSOR = 0,
	CH_BATTERY,
	CH_WORK_OUT,
  CH_THIRD_OUT,
	CH_WORK_IN,
  CH_THIRD_IN,
	CH_CALIB_DAC,
}AdcCH_t;

extern SysInfo_t  g_SysInfo;

//main.c
void HwBeforeSleepInit(void);
void HwAfterSleepInit(void);
//app_task.c
void SleepInitSysVar(void);
void ClearGlobalCount(void);
void IncGlobalCount(void);
uint8_t GetGameStatus(void);
uint32_t GetGlobalCount(void);
//void MainStatePro(void);
void MainStatePro(const SysInfo_t *pInfo);
void ChangeGameStatus(Game_Status nextStatus);
void ChangeMainState(e_Main_State_t nextState);
void TaskProBat(void);
//app_lcd.c
void TaskLcd(void);
//app_analog.c
int16_t ReadTempSensor(void);
uint16_t SenseAdAverage(AdcCH_t ch,uint8_t numbers);
void BatGetLevel(void);
void CalibDaoValue(void);
//app_key.c
void RegButton(void);
void TaskButton(void);
//app_measure.c
void Timer2_5msCallBack(void);
void AdcIntCallBack(void);
void ProSysErr(const SysInfo_t *pInfo);
void Pro_DetInput(void);
void MeasurementValidateStrip(SysInfo_t *pInfo);
void NisoMeasurement(SysInfo_t *pInfo);
void MeasurementConfirmResult(SysInfo_t *pInfo);


#endif