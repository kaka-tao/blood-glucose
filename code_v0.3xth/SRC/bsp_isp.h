#ifndef __BSP_ISP_H__
#define __BSP_ISP_H__

#define	ROM_MEMORY_ADDR         0xE000		
#define MEMORY_NUMBER           1000

void ISPWrite(uint32_t Start_addr,uint8_t data_byte);
uint16_t GetRomMemoryIndex(void);
uint16_t GetRomYear(void);
uint8_t GetRomMonth(void);
uint8_t GetRomDay(void);
uint8_t GetRomHour(void);
uint8_t GetRomMinute(void);
uint8_t GetRomEPYear(void);
uint8_t GetRomEPMonth(void);
uint16_t GetRomLowValue(void);
uint16_t GetRomHighValue(void);
uint16_t GetRomMemberIndex(void);
uint8_t GetRomOther(void);

#endif