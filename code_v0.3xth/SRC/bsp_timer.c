#define __XRAM_SFR_H__
#include <SN8F5959.H>
#include "bsp_timer.h"
#include "type_define.h"


/**********************************************************
* @brief 系统时钟初始化
* @param 
* @return 
**********************************************************/
void InitSysCLK(void)
{       
    /* Clock Switch Select Register */
    CLKSEL  = 0x04;           // Fcpu = Fhosc/8      4M=32M/8 	
    //CLKSEL = 0x06;           // Fcpu = Fhosc/2     16M=32M/2 	
    CLKCMD = 0x69;            // clock switch start    
}
/**********************************************************
* @brief 使能全局中断
* @param 
* @return 
**********************************************************/
void EnableIrq(void)
{
  EAL = 1;
}
/**********************************************************
* @brief 禁止全局中断
* @param 
* @return 
**********************************************************/
void DisableIrq(void)
{
  EAL = 0;
}
/**********************************************************
* @brief TC0初始化  for RTC
* @param 
* @return 
**********************************************************/
void Tc0Init(void)
{
	TF0 = 0;
	T0C = 0;
	T0M = 0x81;						//Ext xtal 32768 Hz
	ET0	= 1;					//ET0	= Enable 
}
/**********************************************************
* @brief TC1初始化 1ms定时
* @param 
* @return 
**********************************************************/
void Tc1Init(void)
{
    TC1M = 0X30;    // TC1 Clock = clock/16   TC1的时钟源为Fcpu

    TC1RL = 0X05;   // AUTO-Reload Register
    TC1RH = 0xFF;
    
    TC1CL = 0X05;   // TC1 COUNTING Register
    TC1CH = 0XFF;
  
	  Enable_1msTimer();
}
/**********************************************************
* @brief TC2初始化 2.5ms定时
* @param 
* @return 
**********************************************************/
void Tc2Init(void)
{
    TC2M = 0X50;    // TC2 Clock = clock/4 =1MHZ   TC2的时钟源为Fcpu =4MHZ
    
    TC2RL = 0X3C;            // AUTO-Reload Register
    TC2RH = 0XF6;
    
    TC2CL = 0X3C;            // TC2 COUNTING Register
    TC2CH = 0XF6;   
              
    Disable_2_5msTimer();
}
/**********************************************************
* @brief 使能 1ms定时
* @param 
* @return 
**********************************************************/
void Enable_1msTimer(void)
{
    TC1M |= 0X80;  // TC1ENB = 1  
    IEN2 |= 0x04;  // TC1IEN = 1  
}
/**********************************************************
* @brief 关闭 2.5ms定时
* @param 
* @return 
**********************************************************/
void Disable_1msTimer(void)
{
    TC1M &= ~0X80;  // TC1ENB = 0  
    IEN2 &= ~0x04;  // TC1IEN = 0   
}

/**********************************************************
* @brief 使能 2.5ms定时
* @param 
* @return 
**********************************************************/
void Enable_2_5msTimer(void)
{
    TC2M |= 0X80;            // TC2ENB = 1  
    IEN2 |= 0x08;            // TC2IEN = 1  
	
#ifdef    DEBUG_TIMER	
	  P14 = 0;P15 = 0;
	  P1UR |= 0x30;
	  P1M |= 0x30;   //P0.4 P0.5as output mode
#endif
}
/**********************************************************
* @brief 关闭 2.5ms定时
* @param 
* @return 
**********************************************************/
void Disable_2_5msTimer(void)
{
    TC2M &= ~0X80;            // TC2ENB = 0  
    IEN2 &= ~0x08;            // TC2IEN = 0  
}

/************************************************
*  定义函数指针
*************************************************/
static void(* pTc1_1msIntCbFunc)(void); 
static void(* pTc2_2_5msIntCbFunc)(void); 
/**********************************************************
* @brief 注册回调函数
* @param pFunc，函数指针变量，接收传入的回调函数地址
* @return 
**********************************************************/
void Reg1msInt(void(* pFunc)(void))
{
	Tc1Init();
  pTc1_1msIntCbFunc = pFunc;
}

void Reg2_5msInt(void(* pFunc)(void))
{
	Tc2Init();
  pTc2_2_5msIntCbFunc = pFunc;
}
/************************************************
*  TC0 interrupt入口   for RTC
*************************************************/
void TC0_ISR(void) interrupt ISRTC0 // Vector @  0x13
{
  
}
/************************************************
*  TC1 interrupt入口   1ms
*************************************************/
void TC1_ISR(void) interrupt ISRTC1 // Vector @  0x13
{
#ifdef    DEBUG_TIMER	
    P14 ^=1;
#endif
    pTc1_1msIntCbFunc();
}

/************************************************
*  TC2 interrupt入口   2.5ms
*************************************************/
void TC2_ISR(void) interrupt ISRTC2 // Vector @  0x13
{
#ifdef    DEBUG_TIMER	
    P15 ^=1;
#endif
    pTc2_2_5msIntCbFunc();
}

