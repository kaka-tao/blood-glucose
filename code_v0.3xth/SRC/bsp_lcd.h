#ifndef __BSP_LCD_H__
#define __BSP_LCD_H__
#include "type_define.h"
typedef enum
{
   E_DIS_OFF = 0,
	E_DIS_ON  = 1,
}e_dismode_t;


void Dis_Seg_S1(e_dismode_t mode);
void Dis_Seg_S2(e_dismode_t mode);
void Dis_Seg_S3(e_dismode_t mode);
void Dis_Seg_S4(e_dismode_t mode);
void Dis_Seg_S5(e_dismode_t mode);
void Dis_Seg_S7(e_dismode_t mode);
void Dis_Seg_S8(e_dismode_t mode);
void Dis_Seg_S9(e_dismode_t mode);
void Dis_Seg_S10(e_dismode_t mode);
void Dis_Seg_S11(e_dismode_t mode);
void Dis_Seg_S12(e_dismode_t mode);
void Dis_Seg_S13(e_dismode_t mode);
void Dis_Seg_S14(e_dismode_t mode);
void Dis_Seg_S15(e_dismode_t mode);
void Dis_Seg_S16(e_dismode_t mode);
void Dis_Seg_S17(e_dismode_t mode);
void Dis_Seg_S18(e_dismode_t mode);
void Dis_Seg_S19(e_dismode_t mode);
void Dis_Seg_S22(e_dismode_t mode);
void Dis_Seg_S23(e_dismode_t mode);
void Dis_Seg_S24(e_dismode_t mode);
void Dis_Seg_S25(e_dismode_t mode);
void Dis_Seg_R1(e_dismode_t mode);
extern void ClearLcdRam(void);
extern void FullLcdRam(void);
void InitLcd(void);
void DisPlayNumber(unsigned char position,unsigned char n);
extern void LCD_delay_ms(uint16_t n);

#endif