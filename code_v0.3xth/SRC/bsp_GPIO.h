#ifndef __BSP_GPIO_H__
#define __BSP_GPIO_H__



#define GPIO_CODE1      (P24)
#define GPIO_CODE2      (P25) 
#define GPIO_CODE3      (P26) 
#define GPIO_CODE4      (P27) 

typedef enum{
  io_code_1 = 0,
	io_code_2,
	io_code_3,
	io_code_4,
	io_code_all,
}GpioCode_t;

typedef enum
{
	Strip_Verify_1 = 0,
	Strip_Verify_2 = 1,
}Strip_VerifyTypeDef;

void GpioInit(void);
void StripSet(GpioCode_t io);
void StripRead(GpioCode_t io, uint8_t *Code);
void VerifyStripCode(uint8_t Strip_Code, Strip_VerifyTypeDef Strip_Verify_n);
uint8_t Is_Strip_Available(uint8_t *Current_Code);

void CloseAllPeripheral(void);
void SysEnterStop(void);
void DelayReadAdc(void);








#endif