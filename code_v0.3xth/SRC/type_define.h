#ifndef __TYPE_DEFINE_H__
#define __TYPE_DEFINE_H__


/* exact-width signed integer types */
typedef signed char         int8_t;
typedef signed int          int16_t;
typedef signed long int     int32_t;

    /* exact-width unsigned integer types */
typedef unsigned char       uint8_t;
typedef unsigned int        uint16_t;
typedef unsigned long  int  uint32_t;

typedef enum {FALSE = 0, TRUE = !FALSE}; 






#endif