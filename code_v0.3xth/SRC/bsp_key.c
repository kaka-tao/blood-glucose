#include <SN8F5959.H>
#include "bsp_key.h"

/************************************************
*  初始化DET引脚  DET -> P0.0
*************************************************/
void InitDetGpio(void)
{
	 P00 = 0;
	 P0UR |= 0x01;
	 P0M &= 0xfe;   //P0.0 as input mode
}
/*******************************************
* 初始化按键     S_Key -> P0.1
              left_Key -> P0.2
             right_Key -> P0.3
*************************************************/
void InitButtonGpio(void)
{
	 P01 = 0;P02 = 0;P03 = 0;
	 P0UR |= 0x0e;
	 P0M &= 0xf1;   //P0.1 P0.2 P0.3 as input mode

//   PEDGE |= 0x08;      // EX1G = 0x10 : INT1 Falling edge trigger (default).    
//   EX1 = 1;            // INT1 isr enable
}

/*******************************************
* 获取DET状态
*************************************************/
uint8_t GetDet_State(void)
{
	return (uint8_t)STRIP_DET;
}
/*******************************************
* 获取按键状态
*************************************************/
uint8_t GetKey_S_State(void)
{
	return (uint8_t)KEY_S;
}
uint8_t GetKey_Left_State(void)
{
	return (uint8_t)KEY_LEFT;
}
uint8_t GetKey_Right_State(void)
{
	return (uint8_t)KEY_RIGHT;
}









///************************************************
//*    INT1 ISR
//*************************************************/
//void INT1_ISR(void) interrupt ISRInt1
//{
//    _nop_();
//}