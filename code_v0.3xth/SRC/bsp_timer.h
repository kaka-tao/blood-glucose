#ifndef __BSP_TIMER_H__
#define __BSP_TIMER_H__


//timer  &  clock
void InitSysCLK(void);
void Tc0Init(void);
void Tc1Init(void);
void Tc2Init(void);
void Reg1msInt(void(* pFunc)(void));
void Reg2_5msInt(void(* pFunc)(void));

void Enable_1msTimer(void);
void Disable_1msTimer(void);
void Enable_2_5msTimer(void);
void Disable_2_5msTimer(void);

void EnableIrq(void);
void DisableIrq(void);


#endif