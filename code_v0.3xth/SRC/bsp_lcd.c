
#include <SN8F5959.H>

#include "bsp_lcd.h"


//-----------------------------------------------------------------------------
//-----------------------------LCD Table DATA---------------------------------//

#define    SA    0x0001    
#define    SB    0x0002    
#define    SC    0x0004
#define    SD    0x0008
#define    SE    0x0800    
#define    SF    0x0200
#define    SG    0x0400    
//#define    SH    0x0800
  

#define LCD_0    SA+SB+SC+SD+SE+SF         // '0'
#define LCD_1    SB+SC                     // '1'
#define LCD_2    SA+SB+SD+SE+SG            // '2'
#define LCD_3    SA+SB+SC+SD+SG            // '3'
#define LCD_4    SF+SG+SB+SC               // '4'
#define LCD_5    SA+SC+SD+SF+SG            // '5'
#define LCD_6    SA+SC+SD+SE+SF+SG         // '6'
#define LCD_7    SA+SB+SC                  // '7'
#define LCD_8    SE+SF+SG+SD+SA+SB+SC      // '8'
#define LCD_9    SF+SG+SD+SA+SB+SC         // '9'
#define LCD_down    SD                     // '_'   10 ..
#define LCD_b    SE+SF+SG+SD+SC            // 'b'   11 ..
#define LCD_C    SD+SE+SG                  // 'C'   12
#define LCD_d    SE+SG+SD+SB+SC            // 'd'   13
#define LCD_E    SE+SF+SG+SA+SD            // 'E'   14 ..
#define LCD_F    SE+SA+SF+SG               // 'F'   15 ..
#define LCD_h    SE+SF+SG+SC               // 'h'   16 ..
#define LCD_H    SE+SF+SG+SB+SC            // 'H'   17
#define LCD_o    SC+SD+SE+SG               // 'o'   18
#define LCD_P    SE+SF+SG+SA+SB            // 'P'   19 ..
#define LCD_L    SD+SE+SF                  // 'L'   20
#define LCD_r    SE+SG                     // 'r'   21
#define LCD_Y    SF+SG+SD+SB+SC            // 'Y'   22
#define LCD_sy   SG                        // '-'   23 ..
#define LCD_t    SD+SE+SF+SG               // 't'   24 ..
#define LCD_T    SA                        // '-'   25 ..
#define LCD_S    SA+SF+SG+SC+SD            // 'S'   26 ..
#define LCD_n    SG+SE+SC                  // 'n'   27 ..
#define LCD_OFF  0x0000                    // 'OFF' 28 ..
#define LCD_ALL  SA+SF+SE+SD+SC+SB+SG      // 'All' 29 ..

//#define ON       1u
//#define OFF      0u

            
void LCD_delay_ms(uint16_t n)
{
    uint16_t i, j;

    for (i=0; i<n; i++) {
        for (j=0; j<430; j++) {
            _nop_();    _nop_();
            _nop_();    _nop_();
            _nop_();    _nop_();
            _nop_();    _nop_();
        }
    }
}

//-----------------------------------------------------------------------------
// Global Variables 
//-----------------------------------------------------------------------------
code uint16_t LCD_Table[30] = {LCD_0,LCD_1,LCD_2,LCD_3,LCD_4,                                         
                          LCD_5,LCD_6,LCD_7,LCD_8,LCD_9,
                          LCD_down,LCD_b,LCD_C,LCD_d,LCD_E,            
                          LCD_F,LCD_h,LCD_H,LCD_o,LCD_P,
                          LCD_L,LCD_r,LCD_Y,LCD_sy,LCD_t,
                          LCD_T,LCD_S,LCD_n,LCD_OFF,LCD_ALL
                         };                        
static uint16_t xdata LCD_RAM _at_ 0xF000;            
static uint16_t *RAM_Point ;                                                               


/**********************************************************
* @brief 清除液晶内部RAM
* @param 
* @return 
**********************************************************/
void ClearLcdRam(void)
{
    uint8_t i;
	RAM_Point = &LCD_RAM;
    for(i = 0; i <= 17; ++i,++RAM_Point)
    {
        *RAM_Point = 0x0000;
		
    }
	RAM_Point = &LCD_RAM;
}

/**********************************************************
* @brief 液晶全显
* @param 
* @return 
**********************************************************/
void FullLcdRam(void)
{
    uint8_t i;
	RAM_Point = &LCD_RAM;
    for(i = 0; i <= 17; ++i,++RAM_Point)
    {
        *RAM_Point = 0xffff;
		
    }
	RAM_Point = &LCD_RAM;
}


/**********************************************************
* @brief 初始化液晶接口
* @param 
* @return 
**********************************************************/
void InitLcd(void)
{
	  RAM_Point = &LCD_RAM;
	  ClearLcdRam();   // Clear LCD RAM
	
    P3CON = 0x00;
    P4CON = 0x00;
    P5CON = 0x00;
    
    LCDM1 &= 0x00;     // Clear LCDM1   
    LCDM1 |= 0x02;     // Enable LCD Pump
    LCD_delay_ms(1);       // Dealy 1m sec    
  //======== LCD Frame selection ======================    
    LCDM1 &= 0xF3;     // LCD Clock=256HZ,Frame=64Hz    @4-COM    
    //LCDM1 |= 0x08;   // LCD Clock=256HZ,Frame=43Hz    @6-COM    
    //LCDM1 |= 0x04;   // LCD Clock=512HZ,Frame=128Hz   @4-COM    
    //LCDM1 |= 0x0C;   // LCD Clock=512HZ,Frame=85Hz    @6-COM    
 
    LCDM2 &= 0x00;     // Clear LCDM2  
  //======== set Low power mode =======================            
    //LCDM2 &= 0xBF;   // Enable Low power Mode @STOP MODE
    LCDM2 |= 0x40;     // Disable LCD low power mode.
  //======== Turn on duty  selection @Low Power Mode====    
    LCDM2 |= 0x30;     // Set Duty = 16*(1/32k)@Frame=128Hz&85Hz , 32*(1/32k)@Frame=64Hz&43Hz
  
  //======== VLCD  selection ==========================        
    //LCDM2 &= 0xF0;   // VLCD=2.6V
    LCDM2 |= 0x01;   // VLCD=2.7V
    //LCDM2 |= 0x02;   // VLCD=2.8V
    //LCDM2 |= 0x03;   // VLCD=2.9V
    //LCDM2 |= 0x04;     // VLCD=3.0V
    //LCDM2 |= 0x05;   // VLCD=3.1V
    //LCDM2 |= 0x06;   // VLCD=3.2V
    //LCDM2 |= 0x07;   // VLCD=3.3V
    //LCDM2 |= 0x08;   // VLCD=3.4V
    //LCDM2 |= 0x09;   // VLCD=3.5V
    //LCDM2 |= 0x0A;   // VLCD=3.6V
    //LCDM2 |= 0x0B;   // VLCD=3.7V
    //LCDM2 |= 0x0C;   // VLCD=3.9V
    //LCDM2 |= 0x0D;   // VLCD=4.1V
    //LCDM2 |= 0x0E;   // VLCD=4.3V
    //LCDM2 |= 0x0F;   // VLCD=4.5V

    LCDM1 |= 0x01;     // Enable LCD 
}
#if 1
/**********************************************************
* @brief 显示一个0-9的数字 
* @param   position：1-11    n：0-29
* @return 
**********************************************************/
void DisPlayNumber(uint8_t position,uint8_t n)
{
	
	uint16_t value;
	RAM_Point = &LCD_RAM;
	
   if(position >= 4 && position <= 11)
   {
		value = RAM_Point[position-4]&0x0100;
		value |= LCD_Table[n];
		RAM_Point[position-4] = value;
	}
   else if(position >= 1 && position <= 3)
   {
		value = RAM_Point[position+8]&0x0100;
		value |= LCD_Table[n];
		RAM_Point[position+8] = value;
	}
}
 
void Dis_Seg_S1(e_dismode_t mode)
{  
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[10] |= 0x0100;
	 else                   RAM_Point[10] &= ~0x0100;
}

void Dis_Seg_S2(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[12] |= 0x0001;
	 else                   RAM_Point[12] &= ~0x0001;	 
}

void Dis_Seg_S3(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[12] |= 0x0002;
	 else                   RAM_Point[12] &= ~0x0002; 
}

void Dis_Seg_S4(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[12] |= 0x0004;
	 else                   RAM_Point[12] &= ~0x0004;
}

void Dis_Seg_S5(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[12] |= 0x0008;
	 else                   RAM_Point[12] &= ~0x0008; 
}

void Dis_Seg_S7(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[9] |= 0x0100;
	 else                   RAM_Point[9] &= ~0x0100;
}

void Dis_Seg_S8(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[12] |= 0x0800;
	 else                   RAM_Point[12] &= ~0x0800;  
}

void Dis_Seg_S9(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[12] |= 0x0200;
	 else                   RAM_Point[12] &= ~0x0200;  
}

void Dis_Seg_S10(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[12] |= 0x0400;
	 else                   RAM_Point[12] &= ~0x0400;
}

void Dis_Seg_S11(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[8] |= 0x0001;
	 else                   RAM_Point[8] &= ~0x0001; 
}

void Dis_Seg_S12(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[8] |= 0x0002;
	 else                   RAM_Point[8] &= ~0x0002;  
}

void Dis_Seg_S13(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[8] |= 0x0004;
	 else                   RAM_Point[8] &= ~0x0004;  
}

void Dis_Seg_S14(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[0] |= 0x0100;
	 else                   RAM_Point[0] &= ~0x0100;  
}

void Dis_Seg_S15(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[1] |= 0x0100;
	 else                   RAM_Point[1] &= ~0x0100;   
}

void Dis_Seg_S16(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[7] |= 0x0100;
	 else                   RAM_Point[7] &= ~0x0100;   
}

void Dis_Seg_S17(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[8] |= 0x0100;
	 else                   RAM_Point[8] &= ~0x0100;  
}

void Dis_Seg_S18(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[8] |= 0x0008;
	 else                   RAM_Point[8] &= ~0x0008;  
}

void Dis_Seg_S19(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[2] |= 0x0100;
	 else                   RAM_Point[2] &= ~0x0100; 
}

void Dis_Seg_S22(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[4] |= 0x0100;
	 else                   RAM_Point[4] &= ~0x0100;  
}

void Dis_Seg_S23(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[6] |= 0x0100;
	 else                   RAM_Point[6] &= ~0x0100; 
}

void Dis_Seg_S24(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[8] |= 0x0400;
	 else                   RAM_Point[8] &= ~0x0400; 
}

void Dis_Seg_S25(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[8] |= 0x0800;
	 else                   RAM_Point[8] &= ~0x0800; 
}

void Dis_Seg_R1(e_dismode_t mode)
{
	RAM_Point = &LCD_RAM;
   if(mode == E_DIS_ON)   RAM_Point[12] |= 0x0100;
	 else                   RAM_Point[12] &= ~0x0100; 
}

#endif