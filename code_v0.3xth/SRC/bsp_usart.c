#include <SN8F5959.H>
#include "bsp_usart.h"


#define SYSUR1_SM0      (0 << 7)    
#define SYSUR1_SM1      (1 << 7)

#define SYSUR1_REN      (1 << 4)
#define SYSUR1_SMOD     (1 << 7)
#define SYSUR1_ES0      (1 << 4)

#define SYSUR1_BD1      (1 << 7)
#define UR_TX_TAB_SIZE  16
#define BufSize         20

static volatile uint8_t xdata u8TxFlag = 0;

/**********************************************************
* @brief 初始化串口
* @param 
* @return 
**********************************************************/
void InitUsart(void)
{
	P22 = 1;                      // Init P22 Status 
	P23 = 0;                      // Init P23 Status
	P2M = (P2M | 0x04) & ~0x08;   // set P22 Output Mode and P23 Input Mode

	//======= UART1 setting =======
	S1CON = SYSUR1_SM1 ; 
	S1CON &= 0xDF;                // Disable multiprocessor communication
	S0CON2 = SYSUR1_BD1;          // Baud mode form internal baud rate

	CD1 = 1;
	S1RELH = 0x03;                // Set baud rate
	//S0RELL = 0xEC;              // baud rate = 57600(CD1 = 1)    
	S1RELL = 0xF6;                // baud rate = 115200(CD1 = 1)    
	
	TEN1 = 1;                     // Enable UART1 TX
  ETS1 = 1;                     // Enable UART1 TX interrupt
  ERS1 = 1;                     // Enable UART1 RX interrupt
}
/**********************************************************
* @brief 串口1发送中断入口
* @param 
* @return 
**********************************************************/
void SYSUart1InterruptTx(void) interrupt ISRUart1Tx
{
    if ( S1CON&0x2 ) {            // TI1
        S1CON &= ~0x2;            // Clear transmit flag
        u8TxFlag = 1;  
    }
}
/**********************************************************
* @brief 串口1接收中断入口
* @param 
* @return 
**********************************************************/
void SYSUart1InterruptRx(void) interrupt ISRUart1Rx
{
    if ( S1CON&0x1 ) {            // RI1
        S1CON &= ~0x1;            // Clear Receive flag
        //u8RxBuf = S1RBUF;
    }
}
/**********************************************************
* @brief 串口1发送1个字节
* @param 
* @return 
**********************************************************/
static void UartTxByte(uint8_t Tx_Data)
{
    S1TBUF = Tx_Data;

    while(!u8TxFlag); 
    u8TxFlag = 0;
}
/**********************************************************
* @brief 16进制转字符串
* @param  l:需要转换的16进制数据   *s：字符串指针
* @return 
**********************************************************/
static void Itoa5x(uint16_t l, uint8_t *s)
{
    s[0]= l/10000+0x30;
    s[1]=(l%10000)/1000+0x30; 
    s[2]=(l%1000)/100+0x30;
    s[3]=(l%100)/10+0x30;
    s[4]= l%10+0x30;
    s[5]= 0;
}
/**********************************************************
* @brief 串口发送字符串
* @param 
* @return 
**********************************************************/
void UartTxString(uint8_t *s)
{
    while(*s)
    {
        UartTxByte(*s++);
    }
}
/**********************************************************
* @brief 串口以字符串方式发送1个16位数据
* @param 
* @return 
**********************************************************/
void UartTxData5x(uint16_t Data)
{	
	uint8_t  s[6];
	
	Itoa5x(Data,s);
	UartTxString(s);
  UartTxString("\r\n");
}
