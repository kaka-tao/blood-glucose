#include <SN8F5959.H>
#include "type_define.h"
#include "bsp_timer.h"

/****************************************************************
整个程序的存储部分
    记忆存储位:
    存储位 0xE000 - 0xFF40   8000个字节   8*1000=1000个数据
****************************************************************/


code uint16_t ROM_YEAR_DATA          _at_ 0xFF60;	    //年
code uint8_t  ROM_MONTH_DATA         _at_ 0xFF62; 		//月
code uint8_t  ROM_DAY_DATA           _at_ 0xFF63; 		//日
code uint8_t  ROM_HOUR_DATA          _at_ 0xFF64; 		//小时
code uint8_t  ROM_MINUTE_DATA        _at_ 0xFF65; 		//分钟
code uint16_t ROM_EP_YEAR_DATA       _at_ 0xFF66;		//试纸过期年
code uint8_t  ROM_EP_MONTH_DATA      _at_ 0xFF68;		//试纸过期月
code uint16_t ROM_LOW_VALUE_DATA     _at_ 0xFF69;		//低血糖设定值
code uint16_t ROM_HIGH_VALUE_DATA    _at_ 0xFF6B;		//高血糖设定值
code uint8_t  ROM_OTHER_DATA         _at_ 0xFF6D;		//0-单位设定 1-时间模式设定 2-声音开关设定
														//3-试纸过期开关设定 4-低血糖开关设定 5-高血糖开关设定
													
code uint16_t ROM_MEMORY_INDEX_DATA  _at_ 0xFF6E; 		//记忆值索引


uint16_t GetRomMemoryIndex(void){
	return ROM_MEMORY_INDEX_DATA;
}	

uint16_t GetRomYear(void){
	return ROM_YEAR_DATA;
}
uint8_t GetRomMonth(void){
	return ROM_MONTH_DATA;
}
uint8_t GetRomDay(void){
	return ROM_DAY_DATA;
}
uint8_t GetRomHour(void){
	return ROM_HOUR_DATA;
}
uint8_t GetRomMinute(void){
	return ROM_MINUTE_DATA;
}
uint8_t GetRomEPYear(void){
	return ROM_EP_YEAR_DATA;
}
uint8_t GetRomEPMonth(void){
	return ROM_EP_MONTH_DATA;
}
uint16_t GetRomLowValue(void){
	return ROM_LOW_VALUE_DATA;
}
uint16_t GetRomHighValue(void){
	return ROM_HIGH_VALUE_DATA;
}
uint16_t GetRomMemberIndex(void){
	return ROM_MEMORY_INDEX_DATA;
}
uint8_t GetRomOther(void){
	return ROM_OTHER_DATA;
}
//t_clock Game_clock;
/************************************************
*  rom地址  0x0000-0x1ffff
*************************************************/
void ISPSetROMAddr(uint32_t u32addr)
{
    // set ROM addr 
    PEROML = ((u32addr & 0x000000FF));          // ISP Target Start ROM address Low byte
    PEROMH = ((u32addr & 0x0000FF00)>>8);       // ISP Target Start ROM address High byte
    PEROMHH = ((u32addr & 0x00030000)>>16);     // ISP Target Start ROM address High High byte
    _nop_();

}

/************************************************
*  写入1字节
*************************************************/
void Write_OneByte(void)
{
    PERAMCNT = 0;     //WR 1 Byte Count        
    // execute Byte Write 
    if(EAL == 0) {
        PECMD = 0x5A;    //page-program
        _nop_(); _nop_();
    }
    else {
        DisableIrq();
        PECMD = 0x5A;    //page-program
        _nop_(); _nop_();
        EnableIrq();
    }    
}
/************************************************
*  设置写入指针地址
*************************************************/
void ISPSetRAMAddr(uint8_t u8addr)
{
    // set RAM addr 
    PERAM = u8addr;    
}

/************************************************
*  设置写入函数
*************************************************/
void ISPWrite(uint32_t Start_addr,uint8_t data_byte)
{
    uint8_t idata u8data_byte[1] = {0};
    uint8_t idata j = 0;
    
    // step 1 : Get data   
    u8data_byte[0] = data_byte;                  // write data for test
        
    // step 2 : Set RAM addr of data
    j = u8data_byte;                        // get start addr
    ISPSetRAMAddr(j);
        
    // step 3 : Set ROM start addr (Range is 0x0000~0x1FFFF)
    ISPSetROMAddr(Start_addr);  
    
    // step 4 : Progarm one byte (1 byte)
    Write_OneByte();
}
