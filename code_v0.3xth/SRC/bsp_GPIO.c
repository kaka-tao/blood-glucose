#include <SN8F5959.H>
#include "type_define.h"
#include "bsp_GPIO.h"

#define  Code_0      ((uint8_t)(0x08|0x04|0x02|0x01))
#define  Code_1      ((uint8_t)(0x04|0x01))
#define  Code_2      ((uint8_t)0x04)
#define  Code_3      ((uint8_t)(0x01|0x02))
#define  Code_4      ((uint8_t)0x02)
#define  Code_5      ((uint8_t)0x01)
#define  Code_6      ((uint8_t)0x0)
#define  Code_7      ((uint8_t)0x08)
#define  Code_8      ((uint8_t)(0x08|0x02))
#define  Code_9      ((uint8_t)(0x08|0x04))
//#define  Code_10      ((uint8_t)0x08)
//#define  Code_11      ((uint8_t)0x0c)

 typedef enum
{
	Strip_Code_0 = 0,
	Strip_Code_1,
	Strip_Code_2,
	Strip_Code_3,
	Strip_Code_4,
	Strip_Code_5,
	Strip_Code_6,
	Strip_Code_7,
	Strip_Code_8,
	Strip_Code_9,
	Strip_Code_Max
}Strip_CodeTypeDef;

 const uint8_t CodeList[]=
{
	Code_0,
	Code_1,
	Code_2,
	Code_3,
	Code_4,
	Code_5,
	Code_6,
	Code_7,
	Code_8,
	Code_9
};

static uint8_t StripCode[2];

/************************************************
*  初始化DET引脚   CODE1 -> P2.4
                  CODE2 -> P2.5
                  CODE3 -> P2.6
                  CODE4 -> P2.7
*************************************************/
static void InitCodeGpio(void)
{
	 GPIO_CODE1 = 0;GPIO_CODE2 = 0;GPIO_CODE3 = 0;GPIO_CODE4 = 0;
	 P2UR |= 0xf0;
	 P2M &= 0x0f;   //P2.4 P2.5 P2.6 P2.7 as input mode

}
/*******************************************
* 初始化硬件GPIO
*************************************************/
void GpioInit(void)
{
	InitCodeGpio();
//  InitDetGpio();
}
/**********************************************************
* @brief  设置试纸条编码引脚
* @param  引脚编号
* @return 
**********************************************************/
void StripSet(GpioCode_t io)
{
	InitCodeGpio();
//  if(io == io_code_1){
//	  P2M |=0x10;GPIO_CODE1 = 0;
//	}  
//  else if(io == io_code_2){
//	  P2M |=0x20;GPIO_CODE2 = 0;
//	}
//  else if(io == io_code_3){
//	  P2M |=0x40;GPIO_CODE3 = 0;
//	}
//  else if(io == io_code_4){
//	  P2M |=0x80;GPIO_CODE4 = 0;
//	}
  if(io == io_code_1){
	  //P2M |=0x10;GPIO_CODE1 = 0;
		P2M |=0x20;GPIO_CODE2 = 0;
		P2M |=0x40;GPIO_CODE3 = 0;
		P2M |=0x80;GPIO_CODE4 = 0;
	}  
  else if(io == io_code_2){
	  //P2M |=0x20;GPIO_CODE2 = 0;
		P2M |=0x10;GPIO_CODE1 = 0;
		P2M |=0x40;GPIO_CODE3 = 0;
		P2M |=0x80;GPIO_CODE4 = 0;
	}
  else if(io == io_code_3){
//	  P2M |=0x40;GPIO_CODE3 = 0;
		P2M |=0x10;GPIO_CODE1 = 0;
		P2M |=0x20;GPIO_CODE2 = 0;
		P2M |=0x80;GPIO_CODE4 = 0;
	}
  else if(io == io_code_4){
//	  P2M |=0x80;GPIO_CODE4 = 0;
		P2M |=0x10;GPIO_CODE1 = 0;
		P2M |=0x20;GPIO_CODE2 = 0;
		P2M |=0x40;GPIO_CODE3 = 0;
	}
}
/**********************************************************
* @brief  读试纸条编码引脚
* @param 
* @return 
**********************************************************/
void StripRead(GpioCode_t io, uint8_t *Code)
{
//	uint8_t temp;
	
//  if(io == io_code_1){
//		 *Code = 0x08;
//		 if(GPIO_CODE2)   *Code |= 0x04;
//		 if(GPIO_CODE3)   *Code |= 0x02;
//		 if(GPIO_CODE4)   *Code |= 0x01;    
//	}
//	else if(io == io_code_2){
//		 temp = 0x04;
//		 if(GPIO_CODE1)   temp |=0x08;
//		 if(GPIO_CODE3)   temp |= 0x02;
//	   if(GPIO_CODE4)   temp |= 0x01;
//		 
//		 *Code &= temp;
//	}
//	else if(io == io_code_3){
//		 temp = 0x02;
//		 if(GPIO_CODE1)   temp |=0x08;
//		 if(GPIO_CODE2)   temp |= 0x04;
//	   if(GPIO_CODE4)   temp |= 0x01;
//		 
//		 *Code &= temp;
//	}
//	else if(io == io_code_4){
//		 temp = 0x01;
//		 if(GPIO_CODE1)   temp |=0x08;
//		 if(GPIO_CODE2)   temp |= 0x04;
//	   if(GPIO_CODE3)   temp |= 0x02;
//		 
//		 *Code &= temp;
//	}
   if(io == io_code_1){
	   if(GPIO_CODE1)
		 {
		    *Code |=0x08;
		 }
	 }
   else if(io == io_code_2){
	   if(GPIO_CODE2)
		 {
		    *Code |=0x04;
		 }
	 }
   else if(io == io_code_3){
	   if(GPIO_CODE3)
		 {
		    *Code |=0x02;
		 }
	 }
   else if(io == io_code_4){
	   if(GPIO_CODE4)
		 {
		    *Code |=0x01;
		 }
	 }	 
}

void VerifyStripCode(uint8_t Strip_Code, Strip_VerifyTypeDef Strip_Verify_n)
{
	Strip_CodeTypeDef i;
	
	for(i = Strip_Code_0; i <= Strip_Code_Max; i++)
	{
		if(CodeList[i] == Strip_Code)
		{
			StripCode[Strip_Verify_n] = i;
			break;
		}
		else if(i == Strip_Code_Max)
		{
			StripCode[Strip_Verify_n] = 0;
		}
	}
}

uint8_t Is_Strip_Available(uint8_t *Current_Code)
{

	if(StripCode[0] == 0)
	{            
		*Current_Code = 0;            

		return FALSE;
	}
	else if(StripCode[0] == StripCode[1])
	{
		*Current_Code = StripCode[0];
		
		return TRUE;
	}
	else if(StripCode[0] >= Strip_Code_Max)
	{
		*Current_Code =0xff;

		return FALSE;
	}
	else
	{
		*Current_Code =0xff;

		return FALSE;
	}
	
}

/************************************************
*    进入低功耗前关闭所有外设
*************************************************/
void CloseAllPeripheral(void)
{
    ADCM1 = 0x00;
    AMPM = 0x00;
    VREG = 0x00;
    I2CCON = 0x00;
    SPCON = 0x00;
    S0CON = 0x00;
    S1CON = 0x00;
    BZRM = 0x00;
    TC2M = 0x00;
    TC1M = 0x00;
    TC0M = 0x00;
    T0M = 0x00;
    OPM1 = 0x00;
    LCDM1 = 0x00;      
    BZRM = 0x00;
    DAM = 0x00;
}
/************************************************
*    进入stop
*************************************************/
void SysEnterStop(void)
{
   STOP();
	 _nop_();_nop_();_nop_();_nop_(); 
}

void DelayReadAdc(void)
{
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
}


