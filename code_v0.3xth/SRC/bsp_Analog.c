#include <SN8F5959.H>
#include "type_define.h"

/************************************************
*  定义函数指针
*************************************************/
static void(* pAdcIntCbFunc)(void); 
/**********************************************************
* @brief 注册Adc回调函数
* @param pFunc，函数指针变量，接收传入的回调函数地址
* @return 
**********************************************************/
void RegAdcIntFunc(void(* pFunc)(void))
{
  pAdcIntCbFunc = pFunc;
}
/************************************************
*  初始化DAC
*************************************************/
static void InitDac(void)
{
    VREG &= 0x00;       // Clear VREG
    VREG |= 0x80;       // Enable Band gap
    //========AVDDR selection==========================    
    VREG &= 0x9F;       // AVDDR=2.2V    
	  VREG |= 0x10;       // Enable AVDDR
    //VREG |= 0x20;     // AVDDR=2.4V
    //VREG |= 0x40;     // AVDDR=2.8V
    //VREG |= 0x60;     // AVDDR=3.2V
    VREG |= 0x0C;       // Enable ACM  ACM = 1V
		VREG |= 0x03;       // Enable AVE  AVE = 2V  使能AVE输出且电压为2V
	  //DAC
    DAM &= 0xCF;        //DAC VREF = AVE       DAC的参考电压
    //DAM |= 0x10;      //DAC VREF = AVDDR 
    //DAM |= 0x20;      //DAC VREF = AVDDR / 2 
    //DAM |= 0x30;      //DAC VREF = AVDDR / 4
    DAM |= 0x80;        //DAC Enable 
}
/**************************************
* bref:设置DAC的输出   12bits
**************************************/
void SetDacValue(uint16_t value)
{	
	DAM &=0xf0;
	DABL &= 0;
	
	DAM |=((uint8_t)(value>>8))&0x0f;
	DABL = (uint8_t)value;
}

// OPA
/**************************************
* DAC连接到OPA1
**************************************/
void DaoConnectOp1p(void)
{
	OPM1 &= ~0x3c;
	OPM1 |= 0x04;
}
/**************************************
* DAC连接到OPA2
**************************************/
void DaoConnectOp2p(void)
{
	OPM1 &= ~0x3c;
	OPM1 |= 0x10;
}
/**************************************
* DAC与OP断开
**************************************/
void DaoDisConnectOp(void)
{
	OPM1 &= ~0x3c;
}
/**************************************
* 初始化OPA
* 模拟开关：SC0、SC3链接用于OPA1    DAO接入运放同相输入端
* 模拟开关：SC11链接用于OPA2        DAO接入运放同相输入端
**************************************/
static void InitOpa(void)
{
    OPM1 &= 0x00;           // OP Initial
    
    OPM1 |= 0x40;           // SC0 connect
	  OPM1 &= 0x7f;           // SC1 disconnect
	  OPM2 &= 0x00;           // SC2-SC9 disconnect
	  OPM2 |= 0x02;           // SC3 connect 
	
	  OP2SW &= 0xfe;          // SC10 disconnect
	  OP2SW |= 0x02;          // SC11 connect
    
    OPM1 |= 0X01;           // OP1 enables & OP1-Buff enable 
    OPM1 |= 0X02;           // OP2 enables & OP2-Buff enable   	
	
    DaoConnectOp1p();
	  DaoConnectOp2p();
}
void DacSetRefWork(uint16_t value)
{
	 DaoConnectOp1p();
	 _nop_();_nop_();_nop_();_nop_();
   SetDacValue(value);
	 _nop_();_nop_();_nop_();_nop_();
}
void DacSetRefThird(uint16_t value)
{
	 DaoConnectOp2p();
	 _nop_();_nop_();_nop_();_nop_();
   SetDacValue(value);
	 _nop_();_nop_();_nop_();_nop_();
}
//ADC
/**************************************
* ADC用于检测电池电压
**************************************/
void AdcConnectBattery(void)
{
	ADCM1 &= ~0x01;	//Disable ADC		
 
	AMPM = 0x20;		//Disable GX Buf + PGIA chopper clock = 5.2kHz(/8) + PGIA Gain = x1 + Enable PGIA Chopper & PGIA
	ADCM1 = 0x1a;		//Disable GR Buf + Vref = 0.5*AVE + ADC Gain x1 + Chopper Enable + "Disable ADC	"(last enable)
	ADCM2 = 0xd0;		//ADC Clock = 333kHz + OSR = 5.2kHz(333k/64) + DRDY flag clear
	
	CHS1 = 0x04;    //+连接到AI5 
	CHS0 = 0x11;    //-连接到AVSS 
	
	ADCM1 |= 0x01;	//Enable ADC	
}
/**************************************
* ADC用于检测温度传感器
**************************************/
void AdcConnectTempSensor(void)
{
	ADCM1 &= ~0x01;	//Disable ADC		
 
	AMPM = 0x20;		//Disable GX Buf + PGIA chopper clock = 5.2kHz(/8) + PGIA Gain = x1 + Enable PGIA Chopper & PGIA
	ADCM1 = 0x1a;		//Disable GR Buf + Vref = 0.5*AVE + ADC Gain x1 + Chopper Enable + "Disable ADC	"(last enable)
	ADCM2 = 0xd0;		//ADC Clock = 333kHz + OSR = 5.2kHz(333k/64) + DRDY flag clear
	
	CHS1 = 0x06;    //+连接到AI7
	CHS0 = 0x11;    //-连接到AVSS 
	
	ADCM1 |= 0x01;	//Enable ADC	
}
/**************************************
* ADC连接到Work
**************************************/
void AdcConnectWorkIn(void)
{
	ADCM1 &= ~0X01;	//Disable ADC		
	
	CHS1 = 0x09;    //连接到OP1N
	CHS0 = 0x11;    //连接到AVSS 
	
	ADCM1 |= 0x01;	//Enable ADC	
}
/**************************************
* ADC连接到Third
**************************************/
void AdcConnectThirdIn(void)
{
	ADCM1 &= ~0X01;	//Disable ADC		
	
	CHS1 = 0x0c;    //连接到OP2N 
	CHS0 = 0x11;    //连接到AVSS 
	
	ADCM1 |= 0x01;	//Enable ADC	
}
/**************************************
* ADC连接到WorkOut
**************************************/
void AdcConnect_Opa1_WorkOut(void)
{
	DaoConnectOp1p();
	
  ADCM1 &= ~0X01;	//Disable ADC	
	
	CHS1 = 0x08;    //连接到AI9 
	CHS0 = 0x11;    //连接到AVSS 
	
	ADCM1 |= 0x01;	//Enable ADC	
}
/**************************************
* ADC连接到ThirdOut
**************************************/
void AdcConnect_Opa2_ThirdOut(void)
{
	DaoConnectOp2p();
	
  ADCM1 &= ~0X01;	//Disable ADC	
	
	CHS1 = 0x05;    //连接到AI6
	CHS0 = 0x11;    //连接到AVSS 
	
	ADCM1 |= 0x01;	//Enable ADC	
}

void AdcConnect_CalibDao(void)
{

	
  ADCM1 &= ~0X01;	//Disable ADC	
	
	CHS1 = 0x0a;    //连接到op1p 
	CHS0 = 0x11;    //连接到AVSS 
	
	ADCM1 |= 0x01;	//Enable ADC	
	
//	SetDacValue(409);
	
	DaoConnectOp1p();
	
	
}
/**************************************
* 使能ADC中断  
**************************************/
void AdcEnableIsr(void)
{
    ADCF = 0;           // Clear ADCF
    IEN2 |= 0x10;       // ADC interrupt enable (EADC)
//    EAL = 1;            // Interrupt enable  
}
/**************************************
* 关闭ADC中断  
**************************************/
void AdcDisableIsr(void)
{
    ADCF = 0;           // Clear ADCF
    IEN2 &= ~0x10;       // ADC interrupt disable (EADC)
//    EAL = 1;            // Interrupt enable  
}
/**************************************
* 单次读取ADC  等效于2V参考电压 12bits结果  （目前采用1V参考电压）
**************************************/
uint16_t ReadAdcSingle(void)
{
  uint32_t ADCValue = 0;

	while(!ADCF);

  ADCF = 0;           // Clear ADC interrupt edge flag (ADCF)
            
  ADCValue = ADCDH;   // Get 24Bit ADC Data output => ( ADCDH, ADCDM, ADCDL )
  ADCValue = ADCValue<<8;
  ADCValue = ADCValue+ADCDM;
  ADCValue = ADCValue<<8;
  ADCValue = ADCValue+ADCDL;
  _nop_();

  return (uint16_t)(ADCValue>>12);  
}
/**************************************
* 硬件模拟电路初始化
**************************************/
void AnalogInit(void)
{
   InitDac();
   InitOpa();
   AdcConnectBattery();
	 AdcDisableIsr();
}
/**************************************
* 中断中读取Adc
**************************************/
uint16_t ReadAdcSingleByInt(void)
{
  uint32_t ADCValue = 0;

	if (ADCF) {   
			ADCF = 0;           // Clear ADC interrupt edge flag (ADCF)
					
	ADCValue = ADCDH;   // Get 24Bit ADC Data output => ( ADCDH, ADCDM, ADCDL )
	ADCValue = ADCValue<<8;
	ADCValue = ADCValue+ADCDM;
	ADCValue = ADCValue<<8;
	ADCValue = ADCValue+ADCDL;
	_nop_();
	} 
	return (uint16_t)(ADCValue>>12);  
}
/**************************************
* 硬件模拟电路初始化
**************************************/
void ADC_ISR(void) interrupt ISRAdc        // Vector @  0x2B
{
  pAdcIntCbFunc();
}



