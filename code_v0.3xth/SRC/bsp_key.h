#ifndef __BSP_KEY_H__
#define __BSP_KEY_H__

#include "type_define.h"

#define BUTTON_UP             ((uint8_t)1)
#define BUTTON_DOWN           ((uint8_t)0)

#define KEY_S           (P01)
#define KEY_LEFT        (P02)
#define KEY_RIGHT       (P03)

#define STRIP_DET       (P00)

void InitDetGpio(void);
void InitButtonGpio(void);

uint8_t GetDet_State(void);
uint8_t GetKey_S_State(void);
uint8_t GetKey_Left_State(void);
uint8_t GetKey_Right_State(void);




#endif