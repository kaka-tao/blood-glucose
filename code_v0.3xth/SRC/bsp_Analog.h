#ifndef __BSP_ANALOG_H__
#define __BSP_ANALOG_H__



//Analog
void AnalogInit(void);

void SetDacValue(uint16_t value);
void DacSetRefWork(uint16_t value);
void DacSetRefThird(uint16_t value);

void AdcEnableIsr(void);
void AdcDisableIsr(void);

void AdcConnect_Opa1_WorkOut(void);
void AdcConnect_Opa2_ThirdOut(void);
void AdcConnectBattery(void);
void AdcConnectTempSensor(void);
void AdcConnectWorkIn(void);
void AdcConnectThirdIn(void);
void AdcConnect_CalibDao(void);

void DaoConnectOp1p(void);
void DaoConnectOp2p(void);
void DaoDisConnectOp(void);

void RegAdcIntFunc(void(* pFunc)(void));
uint16_t ReadAdcSingleByInt(void);
uint16_t ReadAdcSingle(void);







#endif