#ifndef __BSP_USART_H__
#define __BSP_USART_H__
#include "type_define.h"


void InitUsart(void);
/**********************************************************
* @brief 串口发送字符串
* @param 
* @return 
**********************************************************/
void UartTxString(uint8_t *s);
/**********************************************************
* @brief 串口以字符串方式发送1个16位数据
* @param 
* @return 
**********************************************************/
void UartTxData5x(uint16_t Data);














#endif