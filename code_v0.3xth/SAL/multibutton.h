#ifndef __MULTIBUTTON_H__
#define __MULTIBUTTON_H__



//按键控制块
#define     LONG_TICKS             (1500/100) //长按时间
typedef void (*BtnCallback)(void*);

typedef enum {
	  NONE_ButEvent = 0,
    PRESS_DOWN,      //按键按下，每次按下都触发
    PRESS_UP,        //按键弹起，每次松开都触发
    LONG_RRESS_START,//达到长按时间阈值时触发一次
    LONG_PRESS_HOLD, //长按期间一直触发
    NUMBER_event, 
}PressEvent;

typedef struct button
{
	uint8_t      pressFlag        :1;
	uint8_t      longPressFlag    :1;
	uint8_t      startPressFlag   :1;
	
	uint8_t      press_cnts;
	uint8_t      (*Hal_button_level)(void);
	uint8_t      active_gpio_state;
  PressEvent   event;
	BtnCallback  cb[NUMBER_event];
	
	struct button* next;
}button;



void ButtonInit(button *handle, uint8_t(*pin_level)(void), uint8_t active_level);
int  ButtonStart(struct button* handle);
void ButtonStop(struct button* handle);
PressEvent GetButtonEvent(struct button* handle);
void ButtonScan(void);





























#endif