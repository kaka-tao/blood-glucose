#include <string.h>
#include "type_define.h"

#include "multibutton.h"


static xdata struct button* head_handle = NULL;

void ButtonInit(button *handle, uint8_t(*pin_level)(void), uint8_t active_level)
{
    memset(handle, 0, sizeof(button));
    handle->Hal_button_level = pin_level;
    handle->active_gpio_state = active_level;
}

static void ButtonHandler(button *handle)
{
   uint8_t gpio_state = handle->Hal_button_level();
	
   handle->event = NONE_ButEvent;
   if(gpio_state == handle->active_gpio_state)//按键按下
   {
		 if(handle->startPressFlag)//按键上次也处于按下状态
		 {
			 if(++handle->press_cnts >= LONG_TICKS)
			 {
				 handle->press_cnts = LONG_TICKS;//防止数据溢出
				 if(handle->longPressFlag == 0)
				 {
						 handle->longPressFlag = 1;
					 handle->event = LONG_RRESS_START;//获得按键长按事件
				 }
				 else
				 {
						 handle->event = LONG_PRESS_HOLD;//获得按键长按持续事件
				 }
			 }
		 }
		 
     if(handle->pressFlag == 0)//按键从抬起变为按下
	   {
		   handle->event = PRESS_DOWN;//获得按键按下事件
	     handle->startPressFlag = 1;//标记按键开始按下
	   }
     handle->pressFlag = 1;
   }
   else
   {
		 if(handle->pressFlag == 1)//抬起同时按键被按下过
		 {
			 handle->pressFlag = 0;
			 if(handle->longPressFlag == 0){//短按后的弹起
					handle->event = PRESS_UP;   //获得按键短按后弹起事件
			 }
			 else{//长按后的弹起
					handle->longPressFlag = 0;
			 }
		 
		 }
		 else
		 {
			 handle->longPressFlag = 0;
			 handle->startPressFlag = 0;
			 handle->press_cnts = 0;
		 }
   }

}

int ButtonStart(struct button* handle)//遍历单链表，采用在链表头部插节点的方法
{
    struct button* target = head_handle;
    
    while(target) 
    {
        if(target == handle) 
        {
            return -1;  //already exist.
        }
        
        target = target->next;
    }
    
    handle->next = head_handle;
    head_handle = handle;
    
    return 0;
}

void ButtonStop(struct button* handle)
{
    struct button** curr;
    
    for(curr = &head_handle; *curr;) 
    {
        struct button* entry = *curr;
        
        if (entry == handle) 
        {
            *curr = entry->next;
        } 
        else
        {
            curr = &entry->next;
        }
    }
}

PressEvent GetButtonEvent(struct button* handle)
{
    return (PressEvent)(handle->event);
}

void ButtonScan(void)
{
    struct button* target;
    
    for(target = head_handle; target != NULL; target = target->next)
    {
        ButtonHandler(target);
    }
}










